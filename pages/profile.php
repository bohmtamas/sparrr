<?php include './partials/Head.php';?>

	<?php include './partials/header.php';?>

    <div class="pageContent subPageContent">
        <div class="container">

            <h1 class="subPageTitle">Profilom</h1>

            <nav class="profilePageNav">
                <ul>
                    <li class="active"><a href="profile.php">Személyes adatok</a></li>
                    <li><a href="vouchers.php">Vásárlásaim</a></li>
                    <!--<li><a href="balance.php">Egyenlegem</a></li>-->
                </ul>
            </nav>

            <div class="subPageMain full flex">

                <div class="profileFormBox">
					<form id="profileForm">
                        <fieldset>
                            <div class="inputRow">
                                <div class="inputBox">
                                    <label>Keresztnév</label>
                                    <input type="text" name="firstname">
                                </div>
                                <div class="inputBox">
                                    <label>Vezetéknév</label>
                                    <input type="text" name="lastname">
                                </div>
                            </div>
                            <div class="inputRow">
                                <div class="inputBox">
                                    <label>Email cím</label>
                                    <input type="email" name="email">
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <div class="inputRow">
                                <div class="inputBox">
                                    <label>Születési dátum</label>
                                    <select name="birth_year">
                                        <option value="1980">1980</option>
                                        <option value="1981">1981</option>
                                        <option value="1982">1982</option>
                                        <option value="1983">1983</option>
                                        <option value="1984">1984</option>
                                        <option value="1985">1985</option>
                                        <option value="1986">1986</option>
                                        <option value="1987">1987</option>
                                        <option value="1988">1988</option>
                                        <option value="1989">1989</option>
                                        <option value="1990">1990</option>
                                        <option value="1991">1991</option>
                                    </select>
                                </div>
                                <div class="inputBox">
                                    <label>&nbsp;</label>
                                    <select name="birth_month">
                                        <option value="1">január</option>
                                        <option value="2">február</option>
                                        <option value="3">március</option>
                                        <option value="4">április</option>
                                        <option value="5">május</option>
                                        <option value="6">június</option>
                                        <option value="7">július</option>
                                        <option value="8">augusztus</option>
                                        <option value="9">szeptember</option>
                                        <option value="10">október</option>
                                        <option value="11">november</option>
                                        <option value="12">december</option>
                                    </select>
                                </div>
                                <div class="inputBox">
                                    <label>&nbsp;</label>
                                    <select name="birth_day">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                        <option value="13">13</option>
                                        <option value="14">14</option>
                                        <option value="15">15</option>
                                        <option value="16">16</option>
                                        <option value="17">17</option>
                                        <option value="18">18</option>
                                        <option value="19">19</option>
                                        <option value="20">20</option>
                                        <option value="21">21</option>
                                        <option value="22">22</option>
                                        <option value="23">23</option>
                                        <option value="24">24</option>
                                        <option value="25">25</option>
                                        <option value="26">26</option>
                                        <option value="27">27</option>
                                        <option value="28">28</option>
                                        <option value="29">29</option>
                                        <option value="30">30</option>
                                        <option value="31">31</option>
                                    </select>
                                </div>
                            </div>
                            <div class="inputRow">
                                <div class="inputBox">
                                    <label>Lakhely (város)</label>
                                    <input type="text">
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <legend>Jelszó megváltoztatása</legend>
                            <div class="inputRow">
                                <div class="inputBox">
                                    <label>Jelenlegi jelszó</label>
                                    <a class="forgottenPwLink openForgottenPw" href="#userModal" data-toggle='modal'>Elfelejtett jelszó</a>
                                    <input type="password" name="old_password">
                                    <i class="pwVisibilityToggler icon icon-eye"></i>
                                </div>
                            </div>
                            <div class="inputRow">
                                <div class="inputBox">
                                    <label>Új jelszó</label>
                                    <input type="password" name="new_password">
                                    <i class="pwVisibilityToggler icon icon-eye"></i>
                                </div>
                            </div>
                            <div class="inputRow">
                                <div class="inputBox">
                                    <label>Új jelszó mégegyszer</label>
                                    <input type="password" name="new_password_re">
                                    <i class="pwVisibilityToggler icon icon-eye"></i>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <div>
                                <input type="checkbox" name="newsletter" id="newsletterChb" class="chbInput">
                                <label for="newsletterChb" class="chbLabel"><div><i class="icon icon-check"></i></div> Feliratkozom a hírlevélre, hogy elsőként értesüljek az újdonságokról.</label>
                            </div>
                        </fieldset>
						<div class="inputRow">
							<button class="btn greyBtn rounded remove">profil törlése</button>
							<button type="submit" class="btn greenBtn rounded submitBtn">Változtatások mentése</button>
						</div>
					</form>
                </div>

            </div>

        </div>
    </div>

    <?php include './partials/footer.php';?>

	<?php include './partials/modals.php';?> 

    <script src="../assets/js/plugins/jquery/jquery-3.4.1.min.js" defer></script>
    <script src="../assets/js/plugins/jquery/jquery-migrate-1.4.1.min.js" defer></script>
    <script src="../assets/js/plugins/owl-carousel/owl.carousel.min.js" defer></script>
	<script src="../assets/js/plugins/bootstrap-modal/bootstrap-modal-min.js" defer></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
    <script src="../assets/js/plugins/lightpicker/lightpick.js" defer></script>
	<script src="../assets/js/plugins/select2/js/select2.full.min.js" defer></script>        

    <script src="../assets/js/main.js" defer></script>
    <script src="../assets/js/pages/profile.js" defer></script>

<?php include './partials/Foot.php';?>