<?php include './partials/Head.php';?>

    <div class="loginAndRegPageContent partner">
        <figure class="titleBox">
            <a href="/"><img src="../assets/img/logo-white.svg" alt="SPARRR logo"></a>
            <img src="https://picsum.photos/id/155/600/1080" alt="">
            <h2>Van egy jó ajánlatod Vásárlóink számára? Töltsd fel az oldalon!</h2>
        </figure>
        <div class="mainContent">
            <a class="tabletLogo"><img src="../assets/img/logo-color.svg" alt="SPARRR logo"></a>
            <div class="loginContent">
                <h1>Bejelentkezés partnerként</h1>
                <div class="formBox">
                    <form id="loginForm">
                        <fieldset>
                            <div class="inputBox">
                                <input type="email" name="email" placeholder="Email cím">
                            </div>
                            <div class="inputBox">
                                <i class="pwVisibilityToggler icon icon-eye"></i>
                                <input type="password" name="password" placeholder="Jelszó">
                            </div>
                        </fieldset>     
                        <div class="forgottenPwBox">
                            <a class="forgottenPwLink openForgottenPw" href="#userModal" data-toggle='modal'>Elfelejtett jelszó</a>
                        </div>
                        <div>
                            <input type="checkbox" name="stay_logined" id="stayLoginedChb" class="chbInput">
                            <label for="stayLoginedChb" class="chbLabel"><div><i class="icon icon-check"></i></div> Maradjak bejelentkezve</label>
                        </div>
                        <button type="submit" class="btn greenBtn rounded submitBtn">Bejelentkezés</button>
                    </form>
                </div>
                <a href="partner_reg.php" class="changePageLink">Még nincs partner fiókod?</a>
            </div>
            <div class="toRegPageBox">
                <span>Még nincs partner fiókod?</span>
                <a href="partner_reg.php" class="btn greyBtn rounded ">Regisztráció</a>
            </div>
        </div>
        
    </div>

    <?php include './partials/modals.php';?>  

    <script src="../assets/js/plugins/jquery/jquery-3.4.1.min.js" defer></script>
    <script src="../assets/js/plugins/jquery/jquery-migrate-1.4.1.min.js" defer></script>
	<script src="../assets/js/plugins/bootstrap-modal/bootstrap-modal-min.js" defer></script>

	<script src="../assets/js/plugins/select2/js/select2.full.min.js" defer></script>    

	<script src="../assets/js/main.js" defer></script>
	<script src="../assets/js/pages/partner_login.js" defer></script>

<?php include './partials/Foot.php';?>