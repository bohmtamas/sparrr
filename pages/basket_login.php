<?php include './partials/Head.php';?>

	<?php include './partials/header.php';?>

    <div class="pageContent subPageContent">
        <div class="container">

            <div class="splitedWrapper">

                <div class="subPageMain">

                    <nav class="basketMenu">
                        <ul>
                            <li><a><div>1</div><span>Összesítés</span></a></li>
                            <li class="active"><a><div>2</div><span>Bejelentkezés, regisztráció</span></a></li>
                            <li class="disabled"><a><div>3</div><span>Megerősítés és fizetés</span></a></li>
                        </ul>
                    </nav>

                    <h1 class="subPageTitle">Kosár</h1>

                    <div class="basketLoginBox">
                        <div>
                            <h4>Vásárlás regisztrációval</h4>
                            <p>Amennyiben szeretne pár kattintással, fél perc alatt regisztrálni, úgy az oldalon saját profiljában is meg tudja tekinteni érvényes Gutschein-jait. Bejelentkezését, vagy regisztrációját követően azonnal visszajut erre az oldalra.</p>
                            <div class="btnWrap">
                                <a data-toggle="modal" href="#userModal" class="openLogin btn greenBtn rounded">Belépés</a>
                                <a data-toggle="modal" href="#userModal" class="openReg btn greenBtn rounded">Regisztráció</a>
                            </div>
                            <div class="separatorBox">
                                <hr>
                                <span>vagy</span>
                            </div>
                            <div class="socialLoginBox">
                                <button type="button" class="btn whiteBtn fbBtn"> <img src="../assets/img/facebook.svg" alt="facebook login"><span>Belépés Facebookkal</span></button>
                                <button type="button" class="btn whiteBtn googleBtn"> <img src="../assets/img/google.svg" alt="google login"><span>Belépés Google fiókkal</span></button>
                            </div>
                        </div>
                        <div>
                            <h4>Regisztráció nélkül vásárolok</h4>
                            <p>Lehetősége van csak email címe megadásával is vásárolni, azonban ez esetben az oldalon nem lesznek elérhetőek Gutschein-jai.</p>
                            <form>
                                <div class="inputRow">
                                    <div class="inputBox">
                                        <input type="text" name="firstname" placeholder="Keresztnév">
                                    </div>
                                    <div class="inputBox">
                                        <input type="text" name="lastname" placeholder="Vezetéknév">
                                </div>
                                </div>
                                <div class="inputBox">
                                    <input type="email" name="email" placeholder="Email címed">
                                </div>
                                <button type="submit" class="btn greenBtn rounded">Tovább</button>
                            </form>
                        </div>
                    </div>

                </div>

                <aside class="subPageAside right">
    
                    <div class="subOffers">
                        <h5>Ez is érdekelheti</h5>
                        <div class="offersBox owl-carousel">
                            <div class="offerItem">
                                <a href="offer_inner.php">
                                    <figure class="imgBox">
                                        <div class="offerStatusBox">
                                            <div class="statusBox">
                                                <div class="statusItem">
                                                    <img src="../assets/img/badge.svg">
                                                </div>
                                            </div>
                                            <div class="discountBox">
                                                <p>-57%</p>
                                            </div>
                                        </div>
                                        <div class="imgCaroBox">
                                            <img src="https://picsum.photos/id/160/360/220" alt="">
                                        </div>
                                    </figure>
                                    <article class="textBox">
                                        <h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
                                        <div class="offerPriceBox">
                                            <strong class="newPrice">45.000 Ft</strong>
                                            <p class="oldPrice">79.000 Ft</p>
                                        </div>
                                    </article>
                                </a>
                                <div class="distanceBox">
                                    <span>2.8 km</span>
                                    <span class="saveOffer"><i class="icon icon-banner"></i></span>
                                </div>
                            </div>
                            <div class="offerItem">
                                <a href="offer_inner.php">
                                    <figure class="imgBox">
                                        <div class="offerStatusBox">
                                            <div class="statusBox">
                                                <div class="statusItem">
                                                    <img src="../assets/img/badge.svg">
                                                </div>
                                            </div>
                                            <div class="discountBox">
                                                <p>-57%</p>
                                            </div>
                                        </div>
                                        <div class="imgCaroBox">
                                            <img src="https://picsum.photos/id/160/360/220" alt="">
                                        </div>
                                    </figure>
                                    <article class="textBox">
                                        <h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
                                        <div class="offerPriceBox">
                                            <strong class="newPrice">45.000 Ft</strong>
                                            <p class="oldPrice">79.000 Ft</p>
                                        </div>
                                    </article>
                                </a>
                                <div class="distanceBox">
                                    <span>2.8 km</span>
                                    <span class="saveOffer"><i class="icon icon-banner"></i></span>
                                </div>
                            </div>
                            <div class="offerItem">
                                <a href="offer_inner.php">
                                    <figure class="imgBox">
                                        <div class="offerStatusBox">
                                            <div class="statusBox">
                                                <div class="statusItem">
                                                    <img src="../assets/img/badge.svg">
                                                </div>
                                            </div>
                                            <div class="discountBox">
                                                <p>-57%</p>
                                            </div>
                                        </div>
                                        <div class="imgCaroBox">
                                            <img src="https://picsum.photos/id/160/360/220" alt="">
                                        </div>
                                    </figure>
                                    <article class="textBox">
                                        <h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
                                        <div class="offerPriceBox">
                                            <strong class="newPrice">45.000 Ft</strong>
                                            <p class="oldPrice">79.000 Ft</p>
                                        </div>
                                    </article>
                                </a>
                                <div class="distanceBox">
                                    <span>2.8 km</span>
                                    <span class="saveOffer"><i class="icon icon-banner"></i></span>
                                </div>
                            </div>
                            <div class="offerItem">
                                <a href="offer_inner.php">
                                    <figure class="imgBox">
                                        <div class="offerStatusBox">
                                            <div class="statusBox">
                                                <div class="statusItem">
                                                    <img src="../assets/img/badge.svg">
                                                </div>
                                            </div>
                                            <div class="discountBox">
                                                <p>-57%</p>
                                            </div>
                                        </div>
                                        <div class="imgCaroBox">
                                            <img src="https://picsum.photos/id/160/360/220" alt="">
                                        </div>
                                    </figure>
                                    <article class="textBox">
                                        <h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
                                        <div class="offerPriceBox">
                                            <strong class="newPrice">45.000 Ft</strong>
                                            <p class="oldPrice">79.000 Ft</p>
                                        </div>
                                    </article>
                                </a>
                                <div class="distanceBox">
                                    <span>2.8 km</span>
                                    <span class="saveOffer"><i class="icon icon-banner"></i></span>
                                </div>
                            </div>
                            
                        </div>
                    </div>

                </aside>

            </div>

        </div>
    </div>

    <?php include './partials/footer.php';?>

	<?php include './partials/modals.php';?>

    <script src="../assets/js/plugins/jquery/jquery-3.4.1.min.js" defer></script>
    <script src="../assets/js/plugins/jquery/jquery-migrate-1.4.1.min.js" defer></script>
    <script src="../assets/js/plugins/owl-carousel/owl.carousel.min.js" defer></script>
    <script src="../assets/js/plugins/lightslider/js/lightslider.min.js" defer></script>
	<script src="../assets/js/plugins/bootstrap-modal/bootstrap-modal-min.js" defer></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
    <script src="../assets/js/plugins/lightpicker/lightpick.js" defer></script>
	<script src="../assets/js/plugins/select2/js/select2.full.min.js" defer></script>            

	<script src="../assets/js/main.js" defer></script>

<?php include './partials/Foot.php';?>