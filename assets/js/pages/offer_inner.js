var Offer_inner = {}
 
Offer_inner.init = function(){
    this.packageSelect();
    this.quantitySelect();
    this.gallery();
    this.map();
    this.seenOfferCaro();
    this.addToCartAnimation();
}

Offer_inner.addToCartAnimation = function(){

    $("body").on("click", ".addToCartBtn", function(e){

        var btn = $(this);

        btn.addClass("disabled");

        var logoX; var logoY;
        const basket = $(".toTheCartBox a"); var basketX = basket.offset().left + 19; var basketY= basket.offset().top - 11;


        if($(".pageHeader").hasClass("fixPos")){
            console.info("hello");
            basketY = 10;
            logoX = e.originalEvent.clientX - 18; logoY = e.originalEvent.clientY - 18; 
            $("body").append("<img id='cartLogo' style='position: fixed; top:" + logoY + "px; left:" + logoX + "px; z-index: 99999;' src='../assets/img/logo-mobile.svg' />");
        }
        else {
            console.info("hello2");
            logoX = e.originalEvent.pageX - 18; logoY = e.originalEvent.pageY - 18; 
            $("body").append("<img id='cartLogo' style='position: absolute; top:" + logoY + "px; left:" + logoX + "px; z-index: 99999;' src='../assets/img/logo-mobile.svg' />");
        }

        $("#cartLogo").animate({
            left: basketX,
            top: basketY,
            width: 18
          }, 800, function() {
            $("#cartLogo").fadeOut(300, function(){$("#cartLogo").remove(); btn.removeClass("disabled")});
          });

    });

}

Offer_inner.packageSelect = function() {
    $("#packageSelect").select2({
        minimumResultsForSearch: -1
    });
}

Offer_inner.quantitySelect = function() {
    $("#quantitySelect").select2({
        minimumResultsForSearch: -1,
        placeholder: "Mennyiség"
    });
}

Offer_inner.gallery = function() {
		
    if ($(".contentGalleryList").length > 0) {

        $(".contentGalleryList").each(function() {

            $(this).lightSlider({
                gallery:true,
                item:1,
                loop:true,
                thumbItem:5,
                slideMargin:0,
                enableDrag: false,
                currentPagerPosition:'left',
                prevHtml: "<i class='icon icon-arrowLeft'></i>",
                nextHtml: "<i class='icon icon-arrowRight'></i>",
                responsive : [
                    {
                        breakpoint:767,
                        settings: {
                            item:1,
                            thumbItem:0,
                            gallery: false,
                            pager: true,
                            controls: false
                        }
                    }
                ],
                onSliderLoad: function(el) {
                    el.lightGallery({
                        selector: '.contentGalleryList .lslide'
                    });
                }
            });  

        });
        
    }

}

Offer_inner.map = function(){
    var mapCoorX = $("#mapBox").data("map-coor-x"),
        mapCoorY = $("#mapBox").data("map-coor-y"),
        mapZoom = $("#mapBox").data("map-zoom-value");

    var latlng = new google.maps.LatLng(mapCoorX,mapCoorY);

    directionsDisplay = new google.maps.DirectionsRenderer();
    
    var settings = {
        zoom: mapZoom,
        center: latlng,
        scrollwheel: false,
        mapTypeControl: false,
        panControl: false,
        navigationControl: false,
        streetViewControl: false,
        draggable: true,
        clickable: true,
        gestureHandling: "cooperative",
        disableDoubleClickZoom: true,
        zoomControl: true,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.LARGE,
            position: google.maps.ControlPosition.RIGHT_BOTTOM
        },
        mapTypeId: google.maps.MapTypeId.ROADMAP};

    var map = new google.maps.Map(document.getElementById("mapBox"), settings);

    directionsDisplay.setMap(map);
}

Offer_inner.seenOfferCaro = function(){
    if($(".seenOffers").length > 0){

        var seenOffersCaro = $(".seenOffers .offersBox").owlCarousel({
            loop: false,
            items: 4,
            autoplay: true,
            margin: 30,
            dots: false,
            nav: false,
            responsive : {
                300 : {
                    items: 1,
                    loop: false,
                    margin: 20
                },
                500 : {
                    items: 2,
                    loop: false,
                    margin: 20
                },
                768 : {
                    items: 2,
                    loop: false
                },
                1024 : {
                    items: 3,
                    loop: false
                },
                1200: {
                    items: 4,
                    loop: false
                }
            }
        });
        
    }
}

$(document).ready(function(){
    Offer_inner.init();
});