var Faq = {};

Faq.init = function(){
    this.collapse();
}

Faq.collapse = function (){
    $("body").on('click', '.faqList header', function () {

        var li = $(this).parent();
        var opened  = li.children("div").hasClass("opened");

        $(".faqList li").removeClass("active");

        $(".faqList").find(".opened").slideToggle(300).removeClass("opened");

        if (opened == false){
            li.addClass("active").children("div").addClass("opened").delay(300).slideToggle(300, function(){
                Faq.animate(li.offset().top - 150);
            });
        }

    });
}

Faq.animate = function(posY){
    $('html, body').animate({
        scrollTop: posY
    }, 300);
}

$(document).ready(function(){
    Faq.init();
});