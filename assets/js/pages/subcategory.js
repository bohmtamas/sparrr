var Subcategory = {};

Subcategory.init = function(){
    this.mainCaro();
    this.offerTypeSelect();
    this.offerOrderSelect();
}

Subcategory.mainCaro = function (){
    var mainCaro = $("#mainCaro").owlCarousel({
        loop: true,
        items: 1,
        animateOut: "fadeOut",
        autoplay: true
    });
}

Subcategory.offerTypeSelect = function (){
    $("#offerTypeSelect").select2({
        minimumResultsForSearch: -1
    });
}

Subcategory.offerOrderSelect = function (){
    $("#offerOrderSelect").select2({
        minimumResultsForSearch: -1
    });
}

$(document).ready(function(){
    Subcategory.init();
});