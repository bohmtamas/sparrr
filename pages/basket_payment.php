<?php include './partials/Head.php';?>

	<?php include './partials/header.php';?>

    <div class="pageContent subPageContent">
        <div class="container">

            <div class="splitedWrapper">

                <div class="subPageMain">

                    <nav class="basketMenu">
                        <ul>
                            <li><a><div>1</div><span>Összesítés</span></a></li>
                            <li><a><div>2</div><span>Bejelentkezés, regisztráció</span></a></li>
                            <li class="active"><a><div>3</div><span>Megerősítés és fizetés</span></a></li>
                        </ul>
                    </nav>

                    <h1 class="subPageTitle">Kosár</h1>

                    <p class="paymentInfo">Kérem válassza ki az Önnek megfelelő fizetési módot:</p>

                    <div class="paymentContentBox">
                        <ul class="paymentOptions">
                            <li>
                                <input type="radio" name="payment" id="cardRadio" class="radioInput">
                                <label for="cardRadio" class="radioLabel"><div><span></span></div> Bankkártyás fizetés</label>
                            </li>
                            <li>
                                <input type="radio" name="payment" id="paypalRadio" class="radioInput">
                                <label for="paypalRadio" class="radioLabel"><div><span></span></div> PayPal</label>
                            </li>
                            <li>
                                <input type="radio" name="payment" id="klarnaRadio" class="radioInput">
                                <label for="klarnaRadio" class="radioLabel"><div><span></span></div> Klarna</label>
                            </li>
                        </ul>
                    </div>

                    <div class="summaryBox noBorder">
                        <ul class="summaryList">
                            <li class="finalPrice">Fizetendő összeg <span>58 €</span></li>
                        </ul>
                    </div>

                    <nav class="basketNav">
                        <a href="/" class="btn greyBtn rounded">Tovább vásárolok</a>
                        <a class="btn greenBtn rounded">Megveszem</a>
                    </nav>

                </div>

            </div>

        </div>
    </div>

	<?php include './partials/footer.php';?>

	<?php include './partials/modals.php';?>
		
    <script src="../assets/js/plugins/jquery/jquery-3.4.1.min.js" defer></script>
    <script src="../assets/js/plugins/jquery/jquery-migrate-1.4.1.min.js" defer></script>
    <script src="../assets/js/plugins/owl-carousel/owl.carousel.min.js" defer></script>
	<script src="../assets/js/plugins/bootstrap-modal/bootstrap-modal-min.js" defer></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
	<script src="../assets/js/plugins/lightpicker/lightpick.js" defer></script>
	<script src="../assets/js/plugins/select2/js/select2.full.min.js" defer></script>        

	<script src="../assets/js/main.js" defer></script>

<?php include './partials/Foot.php';?>