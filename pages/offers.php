<?php include './partials/Head.php';?>

	<?php include './partials/header.php';?>

    <div class="pageContent subPageContent">
        <div class="container">

            <div class="changeTheOrder">
                <section class="caroSection">
                    
                    <div id="mainCaro" class="owl-carousel">
                        <a class="caroItem">
                            <img src="https://picsum.photos/id/121/1140/120" alt="">
                        </a>
                        <a class="caroItem">
                            <img src="https://picsum.photos/id/122/1140/120" alt="">
                        </a>
                        <a class="caroItem">
                            <img src="https://picsum.photos/id/123/1140/120" alt="">
                        </a>
                    </div>

                </section>

                <div class="filterBox">
                    <form>
                        <div class="inputBox showOnMobile">
                            <label>Ajánlatok</label>
                            <select id="offerTypeSelect">
                                <option value="">Összes (269)</option>
                                <option value="">Fotózás (23)</option>
                                <option value="">Festés (34)</option>
                                <option value="">Gyerek (146)</option>
                            </select>
                        </div>
                        <div class="inputBox">
                            <label>Rendezés</label>
                            <select id="offerOrderSelect">
                                <option value="">Legnagyobb kedvezmény elől</option>
                                <option value="">Újak elől</option>
                                <option value="">Népszerűek elől</option>
                                <option value="">Legolcsóbbak elől</option>
                            </select>
                        </div>
                    </form>
                </div>
            </div>

            

            <div class="splitedWrapper top">

                <aside class="subPageAside left offerCategories">
                    <h1 class="subPageTitle">Ajánlatok</h1>
    
                    <nav class="offersMenu">
                        <ul>
                            <li><a>Összes (269)</a></li>
                            <li><a>Fotózás (23)</a></li>
                            <li><a>Festés (34)</a></li>
                            <li><a>Gyerek (146)</a></li>
                            <li class="active"><a>Adrenalin: repülés, hajózás (69)</a></li>
                            <li><a>Szabadulószoba (16)</a></li>
                            <li><a>Városnézés (42)</a></li>
                            <li><a>Kalandpark (15)</a></li>
                            <li><a>Fürdő, gyógy (35)</a></li>
                            <li><a>Kiállítás (8)</a></li>
                            <li><a>Gasztro (55)</a></li>
                            <li><a>Rendezvény (47)</a></li>
                            <li><a>Sport (38)</a></li>
                            <li><a>Koncert (10)</a></li>
                        </ul>
                    </nav>
    
                </aside>
    
                <div class="subPageMain">

                    <section class="mainOffersSection">

                        <div class="mainOffersBox owl-carousel">

                            <div class="offerItem">
                                <a href="offer_inner.php">
                                    <figure class="imgBox">
                                        <div class="offerStatusBox">
                                            <div class="statusBox"></div>
                                            <div class="discountBox">
                                                <p>-57%</p>
                                            </div>
                                        </div>
                                        <div class="imgCaroBox">
                                            <img src="https://picsum.photos/id/155/360/220" alt="">
                                            <img src="https://picsum.photos/id/156/360/220" alt="">
                                            <img src="https://picsum.photos/id/157/360/220" alt="">
                                        </div>
                                    </figure>
                                    <article class="textBox">
                                        <h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
                                        <div class="offerPriceBox">
                                            <strong class="newPrice">45.000 Ft</strong>
                                            <p class="oldPrice">79.000 Ft</p>
                                        </div>
                                    </article>
                                </a>
                                <div class="distanceBox">
                                    <span>2.8 km</span>
                                    <span class="saveOffer"><i class="icon icon-banner"></i></span>
                                </div>
                            </div>

                            <div class="offerItem">
                                <a href="offer_inner.php">
                                    <figure class="imgBox">
                                        <div class="offerStatusBox">
                                            <div class="statusBox"></div>
                                            <div class="discountBox">
                                                <p>-57%</p>
                                            </div>
                                        </div>
                                        <div class="imgCaroBox">
                                            <img src="https://picsum.photos/id/156/360/220" alt="">
                                        </div>
                                    </figure>
                                    <article class="textBox">
                                        <h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
                                        <div class="offerPriceBox">
                                            <strong class="newPrice">45.000 Ft</strong>
                                            <p class="oldPrice">79.000 Ft</p>
                                        </div>
                                    </article>
                                </a>
                                <div class="distanceBox">
                                    <span>2.8 km</span>
                                    <span class="saveOffer"><i class="icon icon-banner"></i></span>
                                </div>
                            </div>

                        </div>

                    </section>
    
                </div>

            </div>

            <div class="splitedWrapper bottom">

                <div class="subPageMain">
    
                    <div class="offersWrapper">
    
                        <div class="offersBox">
                            <div class="offerItem">
                                <a href="offer_inner.php">
                                    <figure class="imgBox">
                                        <div class="offerStatusBox">
                                            <div class="statusBox">
                                                <div class="statusItem">
                                                    <img src="../assets/img/badge.svg">
                                                </div>
                                            </div>
                                            <div class="discountBox">
                                                <p>-57%</p>
                                            </div>
                                        </div>
                                        <div class="imgCaroBox">
                                            <img src="https://picsum.photos/id/160/360/220" alt="">
                                        </div>
                                    </figure>
                                    <article class="textBox">
                                        <h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
                                        <div class="offerPriceBox">
                                            <strong class="newPrice">45.000 Ft</strong>
                                            <p class="oldPrice">79.000 Ft</p>
                                        </div>
                                    </article>
                                </a>
                                <div class="distanceBox">
                                    <span>2.8 km</span>
                                    <span class="saveOffer"><i class="icon icon-banner"></i></span>
                                </div>
                            </div>
                            <div class="offerItem">
                                <a href="offer_inner.php">
                                    <figure class="imgBox">
                                        <div class="offerStatusBox">
                                            <div class="statusBox">
                                                <div class="statusItem">
                                                    <img src="../assets/img/badge.svg">
                                                </div>
                                            </div>
                                            <div class="discountBox">
                                                <p>-57%</p>
                                            </div>
                                        </div>
                                        <div class="imgCaroBox">
                                            <img src="https://picsum.photos/id/160/360/220" alt="">
                                        </div>
                                    </figure>
                                    <article class="textBox">
                                        <h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
                                        <div class="offerPriceBox">
                                            <strong class="newPrice">45.000 Ft</strong>
                                            <p class="oldPrice">79.000 Ft</p>
                                        </div>
                                    </article>
                                </a>
                                <div class="distanceBox">
                                    <span>2.8 km</span>
                                    <span class="saveOffer"><i class="icon icon-banner"></i></span>
                                </div>
                            </div>
                            <div class="offerItem">
                                <a href="offer_inner.php">
                                    <figure class="imgBox">
                                        <div class="offerStatusBox">
                                            <div class="statusBox">
                                                <div class="statusItem">
                                                    <img src="../assets/img/badge.svg">
                                                </div>
                                            </div>
                                            <div class="discountBox">
                                                <p>-57%</p>
                                            </div>
                                        </div>
                                        <div class="imgCaroBox">
                                            <img src="https://picsum.photos/id/160/360/220" alt="">
                                        </div>
                                    </figure>
                                    <article class="textBox">
                                        <h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
                                        <div class="offerPriceBox">
                                            <strong class="newPrice">45.000 Ft</strong>
                                            <p class="oldPrice">79.000 Ft</p>
                                        </div>
                                    </article>
                                </a>
                                <div class="distanceBox">
                                    <span>2.8 km</span>
                                    <span class="saveOffer"><i class="icon icon-banner"></i></span>
                                </div>
                            </div>
                            <a href="ad.php" class="offerItem typeAd">
                                    
                                <figure class="imgBox">
                                    <img src="https://picsum.photos/id/165/360/220" alt="">
                                </figure>
                                <article class="textBox">
                                    <h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
                                </article>
                                <p>hirdetés</p>
                                
                            </a>
                            <div class="offerItem">
                                <a href="offer_inner.php">
                                    <figure class="imgBox">
                                        <div class="offerStatusBox">
                                            <div class="statusBox">
                                                <div class="statusItem">
                                                    <img src="../assets/img/badge.svg">
                                                </div>
                                            </div>
                                            <div class="discountBox">
                                                <p>-57%</p>
                                            </div>
                                        </div>
                                        <div class="imgCaroBox">
                                            <img src="https://picsum.photos/id/160/360/220" alt="">
                                        </div>
                                    </figure>
                                    <article class="textBox">
                                        <h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
                                        <div class="offerPriceBox">
                                            <strong class="newPrice">45.000 Ft</strong>
                                            <p class="oldPrice">79.000 Ft</p>
                                        </div>
                                    </article>
                                </a>
                                <div class="distanceBox">
                                    <span>2.8 km</span>
                                    <span class="saveOffer"><i class="icon icon-banner"></i></span>
                                </div>
                            </div>
                            <a href="ad.php" class="offerItem typeSeasonable">
                                
                                <figure class="imgBox">
                                    <img src="https://picsum.photos/id/165/260/310" alt="">
                                </figure>
                                <article class="textBox">
                                    <h4>Húsvéti ajánlat</h4>
                                </article>
                            </a>
                            <div class="offerItem">
                                <a href="offer_inner.php">
                                    <figure class="imgBox">
                                        <div class="offerStatusBox">
                                            <div class="statusBox">
                                                <div class="statusItem">
                                                    <img src="../assets/img/badge.svg">
                                                </div>
                                            </div>
                                            <div class="discountBox">
                                                <p>-57%</p>
                                            </div>
                                        </div>
                                        <div class="imgCaroBox">
                                            <img src="https://picsum.photos/id/160/360/220" alt="">
                                        </div>
                                    </figure>
                                    <article class="textBox">
                                        <h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
                                        <div class="offerPriceBox">
                                            <strong class="newPrice">45.000 Ft</strong>
                                            <p class="oldPrice">79.000 Ft</p>
                                        </div>
                                    </article>
                                </a>
                                <div class="distanceBox">
                                    <span>2.8 km</span>
                                    <span class="saveOffer"><i class="icon icon-banner"></i></span>
                                </div>
                            </div>
                            <div class="offerItem">
                                <a href="offer_inner.php">
                                    <figure class="imgBox">
                                        <div class="offerStatusBox">
                                            <div class="statusBox">
                                                <div class="statusItem">
                                                    <img src="../assets/img/badge.svg">
                                                </div>
                                            </div>
                                            <div class="discountBox">
                                                <p>-57%</p>
                                            </div>
                                        </div>
                                        <div class="imgCaroBox">
                                            <img src="https://picsum.photos/id/160/360/220" alt="">
                                        </div>
                                    </figure>
                                    <article class="textBox">
                                        <h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
                                        <div class="offerPriceBox">
                                            <strong class="newPrice">45.000 Ft</strong>
                                            <p class="oldPrice">79.000 Ft</p>
                                        </div>
                                    </article>
                                </a>
                                <div class="distanceBox">
                                    <span>2.8 km</span>
                                    <span class="saveOffer"><i class="icon icon-banner"></i></span>
                                </div>
                            </div>
                            <div class="offerItem">
                                <a href="offer_inner.php">
                                    <figure class="imgBox">
                                        <div class="offerStatusBox">
                                            <div class="statusBox">
                                                <div class="statusItem">
                                                    <img src="../assets/img/badge.svg">
                                                </div>
                                            </div>
                                            <div class="discountBox">
                                                <p>-57%</p>
                                            </div>
                                        </div>
                                        <div class="imgCaroBox">
                                            <img src="https://picsum.photos/id/160/360/220" alt="">
                                        </div>
                                    </figure>
                                    <article class="textBox">
                                        <h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
                                        <div class="offerPriceBox">
                                            <strong class="newPrice">45.000 Ft</strong>
                                            <p class="oldPrice">79.000 Ft</p>
                                        </div>
                                    </article>
                                </a>
                                <div class="distanceBox">
                                    <span>2.8 km</span>
                                    <span class="saveOffer"><i class="icon icon-banner"></i></span>
                                </div>
                            </div>
                        </div>
        
                        <a class="loadMoreBtn loadOffers">Még több ajánlat</a>
        
                    </div>
    
                </div>

                <aside class="subPageAside left">
    
                    <div class="articlesWrapper">
    
                        <h3><a href="articles.php">Cikkajánló</a></h3>
    
                        <div class="articlesBox asideView">
                            <a href="article_inner.php" class="articleItem">
                                <h4>Free Beauty Samples What They Are And How</h4>
                                <p>There are so many different hairstyles for different people and different occasions. If you do an internet search online, you will find an amazing number of web sites and articles dedicated to tha topic…</p>
                            </a>
                            <a href="article_inner.php" class="articleItem">
                                <h4>Free Beauty Samples What They Are And How</h4>
                                <p>There are so many different hairstyles for different people and different occasions. If you do an internet search online, you will find an amazing number of web sites and articles dedicated to tha topic…</p>
                            </a>
                            <a href="article_inner.php" class="articleItem">
                                <h4>Free Beauty Samples What They Are And How</h4>
                                <p>There are so many different hairstyles for different people and different occasions. If you do an internet search online, you will find an amazing number of web sites and articles dedicated to tha topic…</p>
                            </a>
                        </div>

                        <a class="loadMoreBtn loadArticles">További cikkek</a>
    
                    </div>
                </aside>

            </div>

        </div>
    </div>

    <?php include './partials/footer.php';?>

	<?php include './partials/modals.php';?>    

    <script src="../assets/js/plugins/jquery/jquery-3.4.1.min.js" defer></script>
    <script src="../assets/js/plugins/jquery/jquery-migrate-1.4.1.min.js" defer></script>
    <script src="../assets/js/plugins/owl-carousel/owl.carousel.min.js" defer></script>
    <script src="../assets/js/plugins/lightslider/js/lightslider.min.js" defer></script>
	<script src="../assets/js/plugins/bootstrap-modal/bootstrap-modal-min.js" defer></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
    <script src="../assets/js/plugins/lightpicker/lightpick.js" defer></script>
	<script src="../assets/js/plugins/select2/js/select2.full.min.js" defer></script>    

	<script src="../assets/js/main.js" defer></script>
	<script src="../assets/js/pages/offers.js" defer></script>

<?php include './partials/Foot.php';?>