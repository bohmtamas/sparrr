<?php include './partials/Head.php';?>

	<?php include './partials/header.php';?>

    <div class="pageContent subPageContent">
        <div class="container">

            <h1 class="subPageTitle">Profilom</h1>

            <nav class="profilePageNav">
                <ul>
                    <li><a href="vouchers.php">Vásárlásaim</a></li>
                    <li class="active"><a href="balance.php">Egyenlegem</a></li>
                    <li><a href="profile.php">Személyes adatok</a></li>
                </ul>
            </nav>

            <div class="subPageMain full flex">

                <div class="balanceBox">
                    <h3>12.900 Ft</h3>
                    <p>A fel nem használt voucherek összegét később levásárolhatja.</p>
                </div>

            </div>

        </div>
    </div>

	<?php include './partials/footer.php';?>

	<?php include './partials/modals.php';?>

    <script src="../assets/js/plugins/jquery/jquery-3.4.1.min.js" defer></script>
    <script src="../assets/js/plugins/jquery/jquery-migrate-1.4.1.min.js" defer></script>
    <script src="../assets/js/plugins/owl-carousel/owl.carousel.min.js" defer></script>
	<script src="../assets/js/plugins/bootstrap-modal/bootstrap-modal-min.js" defer></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
	<script src="../assets/js/plugins/lightpicker/lightpick.js" defer></script>
	<script src="../assets/js/plugins/select2/js/select2.full.min.js" defer></script>        

	<script src="../assets/js/main.js" defer></script>

<?php include './partials/Foot.php';?>