<?php include './partials/Head.php';?>

	<?php include './partials/partnerHeader.php';?>

    <div class="pageContent subPageContent partnerPageContent">
        <div class="container">

            <h1 class="subPageTitle">Voucherek</h1>

            <a class="addNewVoucherBtn btn greenBtn rounded">Új feltöltése</a>

            <div class="subPageMain full flex">

				<div class="vouchersWrapper">
					<h5>Aktív voucherek</h5>
					<div class="vouchersBox">
						<table class="vouchersTable">
							<thead>
								<th>Kupon</th>
								<th>Érvényes</th>
								<th>Ár</th>
							</thead>
							<tbody>
								<tr>
									<td>
										<div class="offerItem">
											<figure class="imgBox">
												<div class="offerStatusBox">
													<div class="statusBox"></div>
													<div class="discountBox">
														<p>-57%</p>
													</div>
												</div>
												<img src="https://picsum.photos/id/155/360/220" alt="">
											</figure>
											<article class="textBox">
												<h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
											</article>
										</div>
									</td>
									<td>2019.05.12 - 2020.02.02.</td>
									<td>29.900 Ft</td>
								</tr>
								<tr>
									<td>
										<div class="offerItem">
											<figure class="imgBox">
												<div class="offerStatusBox">
													<div class="statusBox"></div>
													<div class="discountBox">
														<p>-57%</p>
													</div>
												</div>
												<img src="https://picsum.photos/id/155/360/220" alt="">
											</figure>
											<article class="textBox">
												<h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
											</article>
										</div>
									</td>
									<td>2019.05.12 - 2020.02.02.</td>
									<td>29.900 Ft</td>
								</tr>
								<tr>
									<td>
										<div class="offerItem">
											<figure class="imgBox">
												<div class="offerStatusBox">
													<div class="statusBox"></div>
													<div class="discountBox">
														<p>-57%</p>
													</div>
												</div>
												<img src="https://picsum.photos/id/155/360/220" alt="">
											</figure>
											<article class="textBox">
												<h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
											</article>
										</div>
									</td>
									<td>2019.05.12 - 2020.02.02.</td>
									<td>29.900 Ft</td>
								</tr>
							</tbody>
						</table>
					</div>
					<h5>Lejárt voucherek</h5>
					<div class="vouchersBox">
						<table class="vouchersTable">
							<thead>
								<th>Kupon</th>
								<th>Érvényes</th>
								<th>Ár</th>
							</thead>
							<tbody>
								<tr>
									<td>
										<div class="offerItem">
											<figure class="imgBox">
												<div class="offerStatusBox">
													<div class="statusBox"></div>
													<div class="discountBox">
														<p>-57%</p>
													</div>
												</div>
												<img src="https://picsum.photos/id/155/360/220" alt="">
											</figure>
											<article class="textBox">
												<h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
											</article>
										</div>
									</td>
									<td class="expired">Lejárt</td>
									<td>29.900 Ft</td>
								</tr>
								<tr>
									<td>
										<div class="offerItem">
											<figure class="imgBox">
												<div class="offerStatusBox">
													<div class="statusBox"></div>
													<div class="discountBox">
														<p>-57%</p>
													</div>
												</div>
												<img src="https://picsum.photos/id/155/360/220" alt="">
											</figure>
											<article class="textBox">
												<h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
											</article>
										</div>
									</td>
									<td class="expired">Lejárt</td>
									<td>29.900 Ft</td>
								</tr>
								<tr>
									<td>
										<div class="offerItem">
											<figure class="imgBox">
												<div class="offerStatusBox">
													<div class="statusBox"></div>
													<div class="discountBox">
														<p>-57%</p>
													</div>
												</div>
												<img src="https://picsum.photos/id/155/360/220" alt="">
											</figure>
											<article class="textBox">
												<h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
											</article>
										</div>
									</td>
									<td class="expired">Lejárt</td>
									<td>29.900 Ft</td>
								</tr>
							</tbody>
						</table>
						<a class="loadMoreBtn loadExpiredVouchers">Régebbi ajánlatok betöltése (+30)</a>
					</div>
				</div>

            </div>

        </div>
    </div>

    <?php include './partials/footer.php';?>

	<?php include './partials/modals.php';?> 
		
    <script src="../assets/js/plugins/jquery/jquery-3.4.1.min.js" defer></script>
    <script src="../assets/js/plugins/jquery/jquery-migrate-1.4.1.min.js" defer></script>
    <script src="../assets/js/plugins/owl-carousel/owl.carousel.min.js" defer></script>
	<script src="../assets/js/plugins/bootstrap-modal/bootstrap-modal-min.js" defer></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
	<script src="../assets/js/plugins/lightpicker/lightpick.js" defer></script>
	<script src="../assets/js/plugins/select2/js/select2.full.min.js" defer></script>        

	<script src="../assets/js/main.js" defer></script>

<?php include './partials/Foot.php';?>