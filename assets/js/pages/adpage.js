var Ad = {}
 
Ad.init = function(){
    this.gallery();
    this.map();
}
Ad.gallery = function() {
		
    if ($(".contentGalleryList").length > 0) {

        $(".contentGalleryList").each(function() {

            $(this).lightSlider({
                gallery:true,
                item:1,
                loop:true,
                thumbItem:5,
                slideMargin:0,
                enableDrag: false,
                currentPagerPosition:'left',
                prevHtml: "<i class='icon icon-arrowLeft'></i>",
                nextHtml: "<i class='icon icon-arrowRight'></i>",
                responsive : [
                    {
                        breakpoint:767,
                        settings: {
                            item:1,
                            thumbItem:0,
                            gallery: false,
                            pager: true,
                            controls: false
                        }
                    }
                ],
                onSliderLoad: function(el) {
                    el.lightGallery({
                        selector: '.contentGalleryList .lslide'
                    });
                }
            });  

        });
        
    }

}

Ad.map = function(){
    var mapCoorX = $("#mapBox").data("map-coor-x"),
        mapCoorY = $("#mapBox").data("map-coor-y"),
        mapZoom = $("#mapBox").data("map-zoom-value");

    var latlng = new google.maps.LatLng(mapCoorX,mapCoorY);

    directionsDisplay = new google.maps.DirectionsRenderer();
    
    var settings = {
        zoom: mapZoom,
        center: latlng,
        scrollwheel: false,
        mapTypeControl: false,
        panControl: false,
        navigationControl: false,
        streetViewControl: false,
        draggable: true,
        clickable: true,
        gestureHandling: "cooperative",
        disableDoubleClickZoom: true,
        zoomControl: true,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.LARGE,
            position: google.maps.ControlPosition.RIGHT_BOTTOM
        },
        mapTypeId: google.maps.MapTypeId.ROADMAP};

    var map = new google.maps.Map(document.getElementById("mapBox"), settings);

    directionsDisplay.setMap(map);
}

$(document).ready(function(){
    Ad.init();
});