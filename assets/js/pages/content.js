var Content = {}
 
Content.init = function(){
    this.gallery();
    this.video();
}

Content.gallery = function() {
		
    if ($(".contentGalleryList").length > 0) {

        $(".contentGalleryList").each(function() {

            $(this).lightSlider({
                gallery:true,
                item:1,
                loop:true,
                thumbItem:5,
                slideMargin:0,
                enableDrag: false,
                currentPagerPosition:'left',
                prevHtml: "<i class='icon icon-arrowLeft'></i>",
                nextHtml: "<i class='icon icon-arrowRight'></i>",
                responsive : [
                    {
                        breakpoint:767,
                        settings: {
                            item:1,
                            thumbItem:0,
                            gallery: false,
                            pager: true,
                            controls: false
                        }
                    }
                ],
                onSliderLoad: function(el) {
                    el.lightGallery({
                        selector: '.contentGalleryList .lslide'
                    });
                }
            });  

        });
        
    }

}

Content.video = function() {

    if ($(".mainTextBox iframe").length > 0) {
        
        $(".mainTextBox iframe").each(function() {

            var src = $(this).attr("src");

            if (src.match("youtube") || src.match("vimeo") || src.match("dailymotion") || src.match("facebook")) {
                $(this).wrap("<div class='videoWrapper'></div>");
            }
            
        });

    }

}

$(document).ready(function(){
    Content.init();
});