var Home = {};

Home.init = function(){
    this.mainCaro();
    this.mainOffersCaro();
}

Home.mainCaro = function (){
    var mainCaro = $("#mainCaro").owlCarousel({
        loop: true,
        items: 1,
        animateOut: "fadeOut",
        autoplay: true
    });
}
Home.mainOffersCaro = function (){

    /*if($(window).width() < 1200){*/
        var mainOffersCaro = $(".mainOffersSection .mainOffersBox").owlCarousel({
            loop: true,
            items: 3,
            autoplay: true,
            margin: 30,
            dots: false,
            responsive : {
                300 : {
                    items: 1,
                    //loop: false,
                    margin: 20
                },
                500 : {
                    items: 2,
                    //loop: false,
                    margin: 20
                },
                768 : {
                    items: 2,
                    //loop: false
                },
                1200: {
                    items: 3,
                    loop: false,
                    mouseDrag: false
                }
            }
        });
    /*}*/
    
}

$(document).ready(function(){
    Home.init();
});