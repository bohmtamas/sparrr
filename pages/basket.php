<?php include './partials/Head.php';?>

	<?php include './partials/header.php';?>

    <div class="pageContent subPageContent">
        <div class="container">

            <div class="splitedWrapper">

                <div class="subPageMain">

                    <nav class="basketMenu">
                        <ul>
                            <li class="active"><a><div>1</div><span>Összesítés</span></a></li>
                            <li class="disabled"><a><div>2</div><span>Bejelentkezés, regisztráció</span></a></li>
                            <li class="disabled"><a><div>3</div><span>Megerősítés és fizetés</span></a></li>
                        </ul>
                    </nav>

                    <h1 class="subPageTitle">Kosár</h1>

                    <div class="basketContentBox">

                        <table class="basketTable">
                            <thead>
                                <th></th>
                                <th>Kupon</th>
                                <th>Ár</th>
                                <th>Mennyiség</th>
                                <th>Összeg</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td  class="removeTd"><a class="removeBasketItem"><i class="icon icon-close"></i></a></td>
                                    <td>
                                        <div href="offer_inner.php" class="offerItem">
                                            <figure class="imgBox">
                                                <div class="offerStatusBox">
                                                    <div class="statusBox"></div>
                                                    <div class="discountBox">
                                                        <p>-57%</p>
                                                    </div>
                                                </div>
                                                <img src="https://picsum.photos/id/155/360/220" alt="">
                                            </figure>
                                            <article class="textBox">
                                                <h4>Romantikus wellness élmény Ráckevén!</h4>

                                                <ul class="tableMobileInfo">
                                                    <li>
                                                        <span>Ár:</span>
                                                        <span>29 €</span>
                                                    </li>
                                                    <li>
                                                        <span>Mennyiség:</span>
                                                        <div class="inputBox">
                                                            <input class="quantityInput" type="text" value="1"/>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <span>Összeg:</span>
                                                        <span>29 €</span>
                                                    </li>
                                                </ul>

                                                <div class="giftWrapper">
                                                    <div class="inputBox">
                                                        <input type="checkbox" name="gift_it" id="giftItChb" class="chbInput">
                                                        <label for="giftItChb" class="chbLabel"><div><i class="icon icon-check"></i></div>Ajándékba adom</label>
                                                    </div>
                                                    
                                                    <div class="giftedDataBox">
                                                        <form>
                                                            <div class="inputBox">
                                                                <input name="gifted_name" type="text" placeholder="Címzett">
                                                            </div>
                                                            <div class="inputBox">
                                                                <input name="gifted_email" type="email" placeholder="Email cím">
                                                            </div>
                                                            <div class="inputBox">
                                                                <textarea name="gifted_msg" placeholder="Üzenet"></textarea>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>

                                            </article>
                                        </div>
                                    </td>
                                    <td class="hideOnTablet"><p>29 €</p></td>
                                    <td class="hideOnTablet">
                                        <div class="inputBox">
                                            <input class="quantityInput" type="text" value="1"/>
                                        </div>
                                    </td>
                                    <td class="hideOnTablet"><p>29 €</p></td>
                                </tr>
                                <tr>
                                    <td class="removeTd"><a class="removeBasketItem"><i class="icon icon-close"></i></a></td>
                                    <td>
                                        <div class="offerItem">
                                            <figure class="imgBox">
                                                <div class="offerStatusBox">
                                                    <div class="statusBox"></div>
                                                    <div class="discountBox">
                                                        <p>-57%</p>
                                                    </div>
                                                </div>
                                                <img src="https://picsum.photos/id/155/360/220" alt="">
                                            </figure>
                                            <article class="textBox">
                                                <h4>Romantikus wellness élmény Ráckevén!</h4>

                                                <ul class="tableMobileInfo">
                                                    <li>
                                                        <span>Ár:</span>
                                                        <span>29 €</span>
                                                    </li>
                                                    <li>
                                                        <span>Mennyiség:</span>
                                                        <div class="inputBox">
                                                            <input class="quantityInput" type="text" value="1"/>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <span>Összeg:</span>
                                                        <span>29 €</span>
                                                    </li>
                                                </ul>

                                                <div class="giftWrapper">
                                                    <div class="inputBox">
                                                        <input type="checkbox" name="gift_it" id="giftItChb" class="chbInput">
                                                        <label for="giftItChb" class="chbLabel"><div><i class="icon icon-check"></i></div>Ajándékba adom</label>
                                                    </div>
                                                    
                                                    <div class="giftedDataBox">
                                                        <form>
                                                            <div class="inputBox">
                                                                <input name="gifted_name" type="text" placeholder="Címzett">
                                                            </div>
                                                            <div class="inputBox">
                                                                <input name="gifted_email" type="email" placeholder="Email cím">
                                                            </div>
                                                            <div class="inputBox">
                                                                <textarea name="gifted_msg" placeholder="Üzenet"></textarea>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>

                                            </article>
                                        </div>
                                    </td>
                                    <td class="hideOnTablet"><p>29 €</p></td>
                                    <td class="hideOnTablet">
                                        <div class="inputBox">
                                            <input class="quantityInput" type="text" value="1"/>
                                        </div>
                                    </td>
                                    <td class="hideOnTablet"><p>29 €</p></td>
                                </tr>
                            </tbody>
                        </table>

                        <div class="summaryBox">
                            <div class="couponBox">
                                <form>
                                    <div class="inputBox">
                                        <input type="text" placeholder="Kuponkód megadása">
                                    </div>
                                    <button class="btn greyBtn rounded addCouponBtn">Hozzáad</button>
                                </form>
                            </div>
                            <ul class="summaryList">
                                <li>Összes megtakarítás<span>32 €</span></li>
                                <li class="finalPrice">Fizetendő összeg <span>58 €</span></li>
                            </ul>
                        </div>

                    </div>

                    <nav class="basketNav">
                        <a href="/" class="btn greyBtn rounded">Tovább vásárolok</a>
                        <a href="basket_payment.php" class="btn greenBtn rounded">Tovább a fizetéshez</a>
                    </nav>

                </div>

                <aside class="subPageAside right">
    
                    <div class="subOffers">
                        <h5>Ez is érdekelheti</h5>
                        <div class="offersBox owl-carousel">
                            <div class="offerItem">
                                <a href="offer_inner.php">
                                    <figure class="imgBox">
                                        <div class="offerStatusBox">
                                            <div class="statusBox">
                                                <div class="statusItem">
                                                    <img src="../assets/img/badge.svg">
                                                </div>
                                            </div>
                                            <div class="discountBox">
                                                <p>-57%</p>
                                            </div>
                                        </div>
                                        <div class="imgCaroBox">
                                            <img src="https://picsum.photos/id/160/360/220" alt="">
                                        </div>
                                    </figure>
                                    <article class="textBox">
                                        <h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
                                        <div class="offerPriceBox">
                                            <strong class="newPrice">45.000 Ft</strong>
                                            <p class="oldPrice">79.000 Ft</p>
                                        </div>
                                    </article>
                                </a>
                                <div class="distanceBox">
                                    <span>2.8 km</span>
                                    <span class="saveOffer"><i class="icon icon-banner"></i></span>
                                </div>
                            </div>
                            <div class="offerItem">
                                <a href="offer_inner.php">
                                    <figure class="imgBox">
                                        <div class="offerStatusBox">
                                            <div class="statusBox">
                                                <div class="statusItem">
                                                    <img src="../assets/img/badge.svg">
                                                </div>
                                            </div>
                                            <div class="discountBox">
                                                <p>-57%</p>
                                            </div>
                                        </div>
                                        <div class="imgCaroBox">
                                            <img src="https://picsum.photos/id/160/360/220" alt="">
                                        </div>
                                    </figure>
                                    <article class="textBox">
                                        <h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
                                        <div class="offerPriceBox">
                                            <strong class="newPrice">45.000 Ft</strong>
                                            <p class="oldPrice">79.000 Ft</p>
                                        </div>
                                    </article>
                                </a>
                                <div class="distanceBox">
                                    <span>2.8 km</span>
                                    <span class="saveOffer"><i class="icon icon-banner"></i></span>
                                </div>
                            </div>
                            <div class="offerItem">
                                <a href="offer_inner.php">
                                    <figure class="imgBox">
                                        <div class="offerStatusBox">
                                            <div class="statusBox">
                                                <div class="statusItem">
                                                    <img src="../assets/img/badge.svg">
                                                </div>
                                            </div>
                                            <div class="discountBox">
                                                <p>-57%</p>
                                            </div>
                                        </div>
                                        <div class="imgCaroBox">
                                            <img src="https://picsum.photos/id/160/360/220" alt="">
                                        </div>
                                    </figure>
                                    <article class="textBox">
                                        <h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
                                        <div class="offerPriceBox">
                                            <strong class="newPrice">45.000 Ft</strong>
                                            <p class="oldPrice">79.000 Ft</p>
                                        </div>
                                    </article>
                                </a>
                                <div class="distanceBox">
                                    <span>2.8 km</span>
                                    <span class="saveOffer"><i class="icon icon-banner"></i></span>
                                </div>
                            </div>
                            <div class="offerItem">
                                <a href="offer_inner.php">
                                    <figure class="imgBox">
                                        <div class="offerStatusBox">
                                            <div class="statusBox">
                                                <div class="statusItem">
                                                    <img src="../assets/img/badge.svg">
                                                </div>
                                            </div>
                                            <div class="discountBox">
                                                <p>-57%</p>
                                            </div>
                                        </div>
                                        <div class="imgCaroBox">
                                            <img src="https://picsum.photos/id/160/360/220" alt="">
                                        </div>
                                    </figure>
                                    <article class="textBox">
                                        <h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
                                        <div class="offerPriceBox">
                                            <strong class="newPrice">45.000 Ft</strong>
                                            <p class="oldPrice">79.000 Ft</p>
                                        </div>
                                    </article>
                                </a>
                                <div class="distanceBox">
                                    <span>2.8 km</span>
                                    <span class="saveOffer"><i class="icon icon-banner"></i></span>
                                </div>
                            </div>
                            
                        </div>
                    </div>

                </aside>

            </div>

        </div>
    </div>

    <?php include './partials/footer.php';?>

	<?php include './partials/modals.php';?>

    <script src="../assets/js/plugins/jquery/jquery-3.4.1.min.js" defer></script>
    <script src="../assets/js/plugins/jquery/jquery-migrate-1.4.1.min.js" defer></script>
    <script src="../assets/js/plugins/owl-carousel/owl.carousel.min.js" defer></script>
    <script src="../assets/js/plugins/lightslider/js/lightslider.min.js" defer></script>
	<script src="../assets/js/plugins/bootstrap-modal/bootstrap-modal-min.js" defer></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
    <script src="../assets/js/plugins/lightpicker/lightpick.js" defer></script>
	<script src="../assets/js/plugins/select2/js/select2.full.min.js" defer></script>            

    <script src="../assets/js/main.js" defer></script>
    <script src="../assets/js/pages/basket.js" defer></script>

<?php include './partials/Foot.php';?>