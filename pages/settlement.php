<?php include './partials/Head.php';?>

	<?php include './partials/partnerHeader.php';?>

    <div class="pageContent subPageContent partnerPageContent">
        <div class="container">

            <h1 class="subPageTitle">Elszámolás</h1>

            <div class="subPageMain full flex">

				<div class="vouchersWrapper">
					<h5>Beváltott voucherek</h5>
					<div class="vouchersBox">
						<table class="vouchersTable lastBorder">
							<thead>
								<th>Kupon</th>
								<th>Beváltás dátuma</th>
								<th>Kód</th>
								<th>Összeg</th>
							</thead>
							<tbody>
								<tr>
									<td>
										<div class="offerItem">
											<figure class="imgBox">
												<div class="offerStatusBox">
													<div class="statusBox"></div>
													<div class="discountBox">
														<p>-57%</p>
													</div>
												</div>
												<img src="https://picsum.photos/id/155/360/220" alt="">
											</figure>
											<article class="textBox">
												<h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
												<ul class="tableMobileInfo">
                                                    <li>
                                                        <span>Beváltás dátuma:</span>
                                                        <span>2019.05.12</span>
                                                    </li>
                                                    <li>
                                                        <span>Kód:</span>
                                                        <span>123456</span>
                                                    </li>
                                                    <li>
                                                        <span>Összeg:</span>
                                                        <span>29 €</span>
                                                    </li>
                                                </ul>
											</article>
										</div>
									</td>
                                    <td class="hideOnTablet">2019.05.12</td>
                                    <td class="hideOnTablet">123456</td>
									<td class="hideOnTablet">29 €</td>
								</tr>
								<tr>
									<td>
										<div class="offerItem">
											<figure class="imgBox">
												<div class="offerStatusBox">
													<div class="statusBox"></div>
													<div class="discountBox">
														<p>-57%</p>
													</div>
												</div>
												<img src="https://picsum.photos/id/155/360/220" alt="">
											</figure>
											<article class="textBox">
												<h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
												<ul class="tableMobileInfo">
                                                    <li>
                                                        <span>Beváltás dátuma:</span>
                                                        <span>2019.05.12</span>
                                                    </li>
                                                    <li>
                                                        <span>Kód:</span>
                                                        <span>123456</span>
                                                    </li>
                                                    <li>
                                                        <span>Összeg:</span>
                                                        <span>29 €</span>
                                                    </li>
                                                </ul>
											</article>
										</div>
									</td>
                                    <td class="hideOnTablet">2019.05.12</td>
                                    <td class="hideOnTablet">123456</td>
									<td class="hideOnTablet">29 €</td>
								</tr>
								<tr>
									<td>
										<div class="offerItem">
											<figure class="imgBox">
												<div class="offerStatusBox">
													<div class="statusBox"></div>
													<div class="discountBox">
														<p>-57%</p>
													</div>
												</div>
												<img src="https://picsum.photos/id/155/360/220" alt="">
											</figure>
											<article class="textBox">
												<h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
												<ul class="tableMobileInfo">
                                                    <li>
                                                        <span>Beváltás dátuma:</span>
                                                        <span>2019.05.12</span>
                                                    </li>
                                                    <li>
                                                        <span>Kód:</span>
                                                        <span>123456</span>
                                                    </li>
                                                    <li>
                                                        <span>Összeg:</span>
                                                        <span>29 €</span>
                                                    </li>
                                                </ul>
											</article>
										</div>
									</td>
                                    <td class="hideOnTablet">2019.05.12</td>
                                    <td class="hideOnTablet">123456</td>
									<td class="hideOnTablet">29 €</td>
								</tr>
							</tbody>
                        </table>
                        <div class="summaryBox">
                            <ul class="summaryList">
                                <li>Összesen:<span>87 €</span></li>
                            </ul>
                        </div>
					</div>
				</div>

            </div>

        </div>
    </div>

    <?php include './partials/footer.php';?>

	<?php include './partials/modals.php';?> 
		
    <script src="../assets/js/plugins/jquery/jquery-3.4.1.min.js" defer></script>
    <script src="../assets/js/plugins/jquery/jquery-migrate-1.4.1.min.js" defer></script>
    <script src="../assets/js/plugins/owl-carousel/owl.carousel.min.js" defer></script>
	<script src="../assets/js/plugins/bootstrap-modal/bootstrap-modal-min.js" defer></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
	<script src="../assets/js/plugins/lightpicker/lightpick.js" defer></script>
	<script src="../assets/js/plugins/select2/js/select2.full.min.js" defer></script>        

	<script src="../assets/js/main.js" defer></script>

<?php include './partials/Foot.php';?>