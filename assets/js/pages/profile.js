var Profile = {};

Profile.init = function(){
    this.profileSelect();
}

Profile.profileSelect = function (){
    $("#profileForm select").select2({
        minimumResultsForSearch: -1
    });
}

$(document).ready(function(){
    Profile.init();
});