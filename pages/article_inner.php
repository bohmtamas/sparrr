<?php include './partials/Head.php';?>

	<?php include './partials/header.php';?>

    <div class="pageContent subPageContent">
        <div class="container">

            <div class="splitedWrapper">

                <div class="subPageMain">

                    <h1 class="subPageTitle">Free Beauty Samples What They Are And How To Find Them Commodo Gravida <span class="articleDate">2019. július 15</span></h1>

                    <article class="mainTextBox">

                        <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed tincidunt metus. Duis imperdiet tortor et eros viverra volutpat. Praesent ante magna, ullamcorper commodo gravida tincidunt, pretium et sem.</h3>
                
                        <br><br>
                
                        <p>Morbi sed metus a augue cursus congue. In vitae urna posuere, congue mi eget, dignissim libero. Aenean venenatis sollicitudin bibendum. Donec sed nisl non neque bibendum lobortis. Cras ex risus, rutrum vel sapien a, laoreet ultricies ipsum. Cras hendrerit imperdiet sem, eu tempor sapien mollis nec. Curabitur ac dolor eleifend tortor iaculis volutpat. In facilisis nec nisl ac egestas. Pellentesque eget luctus sem. Praesent viverra laoreet tortor in posuere. Nam non vestibulum dolor. Vestibulum aliquet augue nec rhoncus convallis. Nunc imperdiet purus eu ante pellentesque condimentum.</p>
                
                        <br><br>

                        <img src="https://picsum.photos/id/155/840/440" alt="gallery">

                        <br><br>

                        <p>Aliquam erat volutpat. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce finibus sapien egestas quam porttitor tempus. Maecenas id massa nec ante facilisis molestie. Nulla posuere nibh sed enim rhoncus egestas. Proin sagittis dictum ultrices. Fusce risus nibh, aliquet et ligula in, viverra fringilla velit. Mauris et arcu iaculis, varius ipsum non, commodo elit. Nulla a odio id sapien convallis sagittis ac quis nisl. Vivamus in est dolor. Pellentesque enim tellus, feugiat at quam eu, tincidunt vestibulum velit.</p>

                        <br><br>

                        <p>Phasellus quis consequat dui. Maecenas condimentum lobortis lorem eu molestie. Sed sed tempus risus, a porttitor nulla. Nam viverra, mi a faucibus gravida, urna leo consequat lacus, id pretium mauris orci eget dolor. Donec eleifend, turpis id venenatis interdum, mi felis egestas purus, ac tincidunt quam dui quis nunc. Curabitur luctus diam in urna maximus luctus. Fusce pharetra aliquam dapibus. Suspendisse egestas felis vitae scelerisque varius. Suspendisse sed lectus enim. Vestibulum non turpis odio. Duis at facilisis est, vel egestas mi. Integer sagittis semper dui ac cursus. Morbi ac euismod quam, a sagittis sapien. Sed efficitur purus orci, non ullamcorper sem ornare ac.</p>

                        <br><br>
            
                        <blockquote>“Morbi gravida ultricies neque sit amet faucibus. Donec euismod lacus at leo placerat hendrerit.”</blockquote>
                
                        <br><br>
                
                        <p>Nante facilisis molestie. Nulla posuere nibh sed enim rhoncus egestas. Proin sagittis dictum ultrices. Fusce risus nibh, aliquet et ligula in, viverra fringilla velit. Mauris et arcu iaculis, varius ipsum non, commodo elit. Nulla a odio id sapien convallis sagittis ac quis nisl. Vivamus in est dolor. Pellentesque enim tellus, feugiat at quam eu, tincidunt vestibulum velit.</p>
                
                    </article>
                </div>

                <aside class="subPageAside right">
    
                    <div class="articlesWrapper">
    
                        <h3><a href="articles.php">Cikkajánló</a></h3>
    
                        <div class="articlesBox asideView">
                            <a href="article_inner.php" class="articleItem">
                                <h4>Free Beauty Samples What They Are And How</h4>
                                <p>There are so many different hairstyles for different people and different occasions. If you do an internet search online, you will find an amazing number of web sites and articles dedicated to tha topic…</p>
                            </a>
                            <a href="article_inner.php" class="articleItem">
                                <h4>Free Beauty Samples What They Are And How</h4>
                                <p>There are so many different hairstyles for different people and different occasions. If you do an internet search online, you will find an amazing number of web sites and articles dedicated to tha topic…</p>
                            </a>
                            <a href="article_inner.php" class="articleItem">
                                <h4>Free Beauty Samples What They Are And How</h4>
                                <p>There are so many different hairstyles for different people and different occasions. If you do an internet search online, you will find an amazing number of web sites and articles dedicated to tha topic…</p>
                            </a>
                        </div>

                        <a class="loadMoreBtn loadArticles">Még több ajánlat</a>
    
                    </div>
                </aside>

            </div>

            <div class="subOffers">
                <h5>Ez is érdekelheti</h5>
                <div class="offersBox owl-carousel">
                    <div class="offerItem">
                        <a href="offer_inner.php">
                            <figure class="imgBox">
                                <div class="offerStatusBox">
                                    <div class="statusBox">
                                        <div class="statusItem">
                                            <img src="../assets/img/badge.svg">
                                        </div>
                                    </div>
                                    <div class="discountBox">
                                        <p>-57%</p>
                                    </div>
                                </div>
                                <div class="imgCaroBox">
                                    <img src="https://picsum.photos/id/160/360/220" alt="">
                                </div>
                            </figure>
                            <article class="textBox">
                                <h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
                                <div class="offerPriceBox">
                                    <strong class="newPrice">45.000 Ft</strong>
                                    <p class="oldPrice">79.000 Ft</p>
                                </div>
                            </article>
                        </a>
                        <div class="distanceBox">
                            <span>2.8 km</span>
                            <span class="saveOffer"><i class="icon icon-banner"></i></span>
                        </div>
                    </div>
                    <div class="offerItem">
                        <a href="offer_inner.php">
                            <figure class="imgBox">
                                <div class="offerStatusBox">
                                    <div class="statusBox">
                                        <div class="statusItem">
                                            <img src="../assets/img/badge.svg">
                                        </div>
                                    </div>
                                    <div class="discountBox">
                                        <p>-57%</p>
                                    </div>
                                </div>
                                <div class="imgCaroBox">
                                    <img src="https://picsum.photos/id/160/360/220" alt="">
                                </div>
                            </figure>
                            <article class="textBox">
                                <h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
                                <div class="offerPriceBox">
                                    <strong class="newPrice">45.000 Ft</strong>
                                    <p class="oldPrice">79.000 Ft</p>
                                </div>
                            </article>
                        </a>
                        <div class="distanceBox">
                            <span>2.8 km</span>
                            <span class="saveOffer"><i class="icon icon-banner"></i></span>
                        </div>
                    </div>
                    <div class="offerItem">
                        <a href="offer_inner.php">
                            <figure class="imgBox">
                                <div class="offerStatusBox">
                                    <div class="statusBox">
                                        <div class="statusItem">
                                            <img src="../assets/img/badge.svg">
                                        </div>
                                    </div>
                                    <div class="discountBox">
                                        <p>-57%</p>
                                    </div>
                                </div>
                                <div class="imgCaroBox">
                                    <img src="https://picsum.photos/id/160/360/220" alt="">
                                </div>
                            </figure>
                            <article class="textBox">
                                <h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
                                <div class="offerPriceBox">
                                    <strong class="newPrice">45.000 Ft</strong>
                                    <p class="oldPrice">79.000 Ft</p>
                                </div>
                            </article>
                        </a>
                        <div class="distanceBox">
                            <span>2.8 km</span>
                            <span class="saveOffer"><i class="icon icon-banner"></i></span>
                        </div>
                    </div>
                    <div class="offerItem">
                        <a href="offer_inner.php">
                            <figure class="imgBox">
                                <div class="offerStatusBox">
                                    <div class="statusBox">
                                        <div class="statusItem">
                                            <img src="../assets/img/badge.svg">
                                        </div>
                                    </div>
                                    <div class="discountBox">
                                        <p>-57%</p>
                                    </div>
                                </div>
                                <div class="imgCaroBox">
                                    <img src="https://picsum.photos/id/160/360/220" alt="">
                                </div>
                            </figure>
                            <article class="textBox">
                                <h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
                                <div class="offerPriceBox">
                                    <strong class="newPrice">45.000 Ft</strong>
                                    <p class="oldPrice">79.000 Ft</p>
                                </div>
                            </article>
                        </a>
                        <div class="distanceBox">
                            <span>2.8 km</span>
                            <span class="saveOffer"><i class="icon icon-banner"></i></span>
                        </div>
                    </div>
                    
                </div>
            </div>

        </div>
    </div>

    <?php include './partials/footer.php';?>

    <?php include './partials/modals.php';?>
    
    <script src="../assets/js/plugins/jquery/jquery-3.4.1.min.js" defer></script>
    <script src="../assets/js/plugins/jquery/jquery-migrate-1.4.1.min.js" defer></script>
    <script src="../assets/js/plugins/owl-carousel/owl.carousel.min.js" defer></script>
    <script src="../assets/js/plugins/lightslider/js/lightslider.min.js" defer></script>
	<script src="../assets/js/plugins/bootstrap-modal/bootstrap-modal-min.js" defer></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
    <script src="../assets/js/plugins/lightpicker/lightpick.js" defer></script>
	<script src="../assets/js/plugins/select2/js/select2.full.min.js" defer></script>                 
    <script src="../assets/js/plugins/lightgallery/js/lightgallery.min.js" defer></script>                

	<script src="../assets/js/main.js" defer></script>
	<script src="../assets/js/pages/content.js" defer></script>

<?php include './partials/Foot.php';?>