var Content = {}
 
Content.init = function(){
    this.checkVoucher();
}

Content.checkVoucher = function() {
		
    $(".checkBtn").click(function(){
        if(Math.floor(Math.random() * 10) > 4){
            $(".voucherCheckBox .inputBox").addClass("ok");
            $(".confirmBtn").removeClass("disabled");
        }
        else {
            $(".voucherCheckBox .inputBox").addClass("wrong");
            $(".confirmBtn").addClass("disabled");
        }
        $(".voucherCheckBox").removeClass("firstStep").addClass("secondStep");
        $(".voucherCheckBox input").attr("readonly",true);
    });
    $(".confirmBtn").click(function(){
        $(".voucherCheckBox").removeClass("secondStep").addClass("thirdStep");
        $(".confirmBtn").addClass("disabled");
    });
    $(".voucherBackBtn").click(function(){
        $(".voucherCheckBox").removeClass("thirdStep").removeClass("secondStep").addClass("firstStep");
        $(".voucherCheckBox .inputBox").removeClass("ok").removeClass("wrong");
        $(".voucherCheckBox input").removeAttr("readonly");
        $(".voucherCheckBox form").trigger("reset");
        $(".voucherCheckBox input").focus();
    });

}

$(document).ready(function(){
    Content.init();
});