<header class="pageHeader">
    <div class="mainHeader partnerHeader">
        <div class="container clearfix">
            <button class="menuOpen">
                <span></span>
            </button>
            <a href="/pages" class="mainLogo">
                <img class="desktopLogo" src="../assets/img/logo.svg" alt="Sparen im Urlaub logo">
                <img class="mobileLogo" src="../assets/img/logo-mobile.svg" alt="Sparen im Urlaub logo">
            </a>
            <div class="menuBox">
                <header>
                    <a href="/pages"><img class="mobileLogo" src="../assets/img/logo-mobile-white.svg" alt="Sparen im Urlaub logo"></a>
                </header>
                <!-- HA BE VAN JELENTKEZVE -->
                <div class="itsMeBox">
                    <h4>Kertész Márk</h4>
                    <p>mark.kertesz01@gmail.com</p>
                </div>
                <!-- HA NINCS BEJELENTKEZVE -->
                <nav class="mobileMenu">
                    <ul>
                        <li><a href="voucher_check.php">Voucher ellenőrzés<i class="icon icon-arrowRight"></i></a></li>
                    </ul>
                </nav>
                <nav class="mobileMenu">
                    <ul>
                        <li><a href="about_us.php">Rólunk<i class="icon icon-arrowRight"></i></a></li>
                        <li><a href="how_we_work.php">Hogyan működünk?<i class="icon icon-arrowRight"></i></a></li>
                        <li><a href="contact.php">Kapcsolat<i class="icon icon-arrowRight"></i></a></li>
                    </ul>
                </nav>
                <nav class="mobileMenu">
                    <ul>
                        <li><a>Kijelentkezés<i class="icon icon-arrowRight"></i></a></li>
                    </ul>
                </nav>
            </div>
            <div class="headerActions">

                <div>
                    <nav class="partnerMenu">
                        <ul>
                            <li class="active"><a href="voucher_check.php">Voucher ellenőrzés</a></li>
                            <!--
                            <li><a href="settlement.php">Elszámolás</a></li>
                            <li ><a href="partner_vouchers.php">Voucherek</a></li>
                            -->
                        </ul>
                    </nav>
                </div>

                <div class="loginBox dropdownParent">
                    <a class="dropdownOpener"><i class="icon icon-profile"></i></a>
                    <div class="profileDropdown dropdown">
                        <div class="itsMeBox">
                            <h4>Kertész Márk</h4>
                            <p>mark.kertesz01@gmail.com</p>
                        </div>
                        <ul class="profileMenu">
                            <!--
                            <li><a>Voucher ellenőrzés</a></li>
                            <li><a>Elszámolás</a></li>
                            <li><a>Voucherek</a></li>
                            <li><a>Személyes adatok</a></li>
                            <li class="logoutLink"><a>Kijelentkezés</a></li>
                            -->
                            <li><a>Kijelentkezés</a></li>
                        </ul>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>