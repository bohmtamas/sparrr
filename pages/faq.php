<?php include './partials/Head.php';?>

	<?php include './partials/header.php';?>

    <div class="pageContent subPageContent">
        <div class="container">

            <div class="splitedWrapper">

                <div class="subPageMain">

                    <h1 class="subPageTitle">GYIK</h1>

                    <ul class="faqList">

                        <li>

                            <header>
                                <h3>1. Mi történik akkor, ha a vásárló nem tudja felhasználni a Gutschein-t?</h3>
                                <i class="icon icon-arrowDown"></i>
                            </header>

                            <div class="hideBox">

                                <article class="mainTextBox">
                                    <p>A vásárló számára, amennyiben mégsem tudja felhasználni a megvásárolt Gutschein-t és ezt jelzi a Sqaure One felé, úgy egy kuponkódot biztosítunk, amelynek értékét egy későbbi vásárlása alkalmával tudja felhasználni az eredeti ajánlat megvásárlásától számított 3 éven belül.</p>
                                </article>

                            </div>

                        </li>

                        <li>

                            <header>
                                <h3>2. Az ajánlat leírásánál szerepel, hogy a vásárlónak szükséges időpontot foglalnia. Hogyan tudom ezt megtenni?</h3>
                                <i class="icon icon-arrowDown"></i>
                            </header>

                            <div class="hideBox">

                                <article class="mainTextBox">
                                    <p>Az email, amiben a Gutschein-ját megkapja, tartalmaz egy linket, amelyre kattintva egy kapcsolatfelvételi felületre vezetjük át. Itt megadhatja az Ön számára alkalmas időpontokat a foglaláshoz, amelyre a szolgáltatóhely miharamabb visszajelez.</p>
                                </article>

                            </div>

                        </li>
                        
                    </ul>

                </div>

                <aside class="subPageAside right">
    
                    <div class="articlesWrapper">
    
                        <h3><a href="articles.php">Cikkajánló</a></h3>
    
                        <div class="articlesBox asideView">
                            <a href="article_inner.php" class="articleItem">
                                <h4>Free Beauty Samples What They Are And How</h4>
                                <p>There are so many different hairstyles for different people and different occasions. If you do an internet search online, you will find an amazing number of web sites and articles dedicated to tha topic…</p>
                            </a>
                            <a href="article_inner.php" class="articleItem">
                                <h4>Free Beauty Samples What They Are And How</h4>
                                <p>There are so many different hairstyles for different people and different occasions. If you do an internet search online, you will find an amazing number of web sites and articles dedicated to tha topic…</p>
                            </a>
                            <a href="article_inner.php" class="articleItem">
                                <h4>Free Beauty Samples What They Are And How</h4>
                                <p>There are so many different hairstyles for different people and different occasions. If you do an internet search online, you will find an amazing number of web sites and articles dedicated to tha topic…</p>
                            </a>
                        </div>

                        <a class="loadMoreBtn loadArticles">További cikkek</a>
    
                    </div>
                </aside>

            </div>

            <div class="subOffers">
                <h5>Ez is érdekelheti</h5>
                <div class="offersBox owl-carousel">
                    <div class="offerItem">
                        <a href="offer_inner.php">
                            <figure class="imgBox">
                                <div class="offerStatusBox">
                                    <div class="statusBox">
                                        <div class="statusItem">
                                            <img src="../assets/img/badge.svg">
                                        </div>
                                    </div>
                                    <div class="discountBox">
                                        <p>-57%</p>
                                    </div>
                                </div>
                                <div class="imgCaroBox">
                                    <img src="https://picsum.photos/id/160/360/220" alt="">
                                </div>
                            </figure>
                            <article class="textBox">
                                <h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
                                <div class="offerPriceBox">
                                    <strong class="newPrice">45.000 Ft</strong>
                                    <p class="oldPrice">79.000 Ft</p>
                                </div>
                            </article>
                        </a>
                        <div class="distanceBox">
                            <span>2.8 km</span>
                            <span class="saveOffer"><i class="icon icon-banner"></i></span>
                        </div>
                    </div>
                    <div class="offerItem">
                        <a href="offer_inner.php">
                            <figure class="imgBox">
                                <div class="offerStatusBox">
                                    <div class="statusBox">
                                        <div class="statusItem">
                                            <img src="../assets/img/badge.svg">
                                        </div>
                                    </div>
                                    <div class="discountBox">
                                        <p>-57%</p>
                                    </div>
                                </div>
                                <div class="imgCaroBox">
                                    <img src="https://picsum.photos/id/160/360/220" alt="">
                                </div>
                            </figure>
                            <article class="textBox">
                                <h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
                                <div class="offerPriceBox">
                                    <strong class="newPrice">45.000 Ft</strong>
                                    <p class="oldPrice">79.000 Ft</p>
                                </div>
                            </article>
                        </a>
                        <div class="distanceBox">
                            <span>2.8 km</span>
                            <span class="saveOffer"><i class="icon icon-banner"></i></span>
                        </div>
                    </div>
                    <div class="offerItem">
                        <a href="offer_inner.php">
                            <figure class="imgBox">
                                <div class="offerStatusBox">
                                    <div class="statusBox">
                                        <div class="statusItem">
                                            <img src="../assets/img/badge.svg">
                                        </div>
                                    </div>
                                    <div class="discountBox">
                                        <p>-57%</p>
                                    </div>
                                </div>
                                <div class="imgCaroBox">
                                    <img src="https://picsum.photos/id/160/360/220" alt="">
                                </div>
                            </figure>
                            <article class="textBox">
                                <h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
                                <div class="offerPriceBox">
                                    <strong class="newPrice">45.000 Ft</strong>
                                    <p class="oldPrice">79.000 Ft</p>
                                </div>
                            </article>
                        </a>
                        <div class="distanceBox">
                            <span>2.8 km</span>
                            <span class="saveOffer"><i class="icon icon-banner"></i></span>
                        </div>
                    </div>
                    <div class="offerItem">
                        <a href="offer_inner.php">
                            <figure class="imgBox">
                                <div class="offerStatusBox">
                                    <div class="statusBox">
                                        <div class="statusItem">
                                            <img src="../assets/img/badge.svg">
                                        </div>
                                    </div>
                                    <div class="discountBox">
                                        <p>-57%</p>
                                    </div>
                                </div>
                                <div class="imgCaroBox">
                                    <img src="https://picsum.photos/id/160/360/220" alt="">
                                </div>
                            </figure>
                            <article class="textBox">
                                <h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
                                <div class="offerPriceBox">
                                    <strong class="newPrice">45.000 Ft</strong>
                                    <p class="oldPrice">79.000 Ft</p>
                                </div>
                            </article>
                        </a>
                        <div class="distanceBox">
                            <span>2.8 km</span>
                            <span class="saveOffer"><i class="icon icon-banner"></i></span>
                        </div>
                    </div>
                    
                </div>
            </div>

        </div>
    </div>

    <?php include './partials/footer.php';?>

    <?php include './partials/modals.php';?>
    
    <script src="../assets/js/plugins/jquery/jquery-3.4.1.min.js" defer></script>
    <script src="../assets/js/plugins/jquery/jquery-migrate-1.4.1.min.js" defer></script>
    <script src="../assets/js/plugins/owl-carousel/owl.carousel.min.js" defer></script>
    <script src="../assets/js/plugins/lightslider/js/lightslider.min.js" defer></script>
	<script src="../assets/js/plugins/bootstrap-modal/bootstrap-modal-min.js" defer></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
    <script src="../assets/js/plugins/lightpicker/lightpick.js" defer></script>
	<script src="../assets/js/plugins/select2/js/select2.full.min.js" defer></script>            

    <script src="../assets/js/main.js" defer></script>
    <script src="../assets/js/pages/faq.js" defer></script>

<?php include './partials/Foot.php';?>