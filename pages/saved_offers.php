<?php include './partials/Head.php';?>

	<?php include './partials/header.php';?>

    <div class="pageContent subPageContent">
        <div class="container">

            <div class="splitedWrapper">

                <div class="subPageMain">

                    <h1 class="subPageTitle">Mentett ajánlataim</h1>

                    <div class="offersWrapper">
    
                        <div class="offersBox">
                            <div class="offerItem">
                                <a href="offer_inner.php">
                                    <figure class="imgBox">
                                        <div class="offerStatusBox">
                                            <div class="statusBox">
                                                <div class="statusItem">
                                                    <img src="../assets/img/badge.svg">
                                                </div>
                                            </div>
                                            <div class="discountBox">
                                                <p>-57%</p>
                                            </div>
                                        </div>
                                        <div class="imgCaroBox">
                                            <img src="https://picsum.photos/id/160/360/220" alt="">
                                        </div>
                                    </figure>
                                    <article class="textBox">
                                        <h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
                                        <div class="offerPriceBox">
                                            <strong class="newPrice">45.000 Ft</strong>
                                            <p class="oldPrice">79.000 Ft</p>
                                        </div>
                                    </article>
                                </a>
                                <div class="distanceBox">
                                    <span>2.8 km</span>
                                    <span class="saveOffer saved"><i class="icon icon-banner"></i></span>
                                </div>
                            </div>
                            <div class="offerItem">
                                <a href="offer_inner.php">
                                    <figure class="imgBox">
                                        <div class="offerStatusBox">
                                            <div class="statusBox">
                                                <div class="statusItem">
                                                    <img src="../assets/img/badge.svg">
                                                </div>
                                            </div>
                                            <div class="discountBox">
                                                <p>-57%</p>
                                            </div>
                                        </div>
                                        <div class="imgCaroBox">
                                            <img src="https://picsum.photos/id/160/360/220" alt="">
                                        </div>
                                    </figure>
                                    <article class="textBox">
                                        <h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
                                        <div class="offerPriceBox">
                                            <strong class="newPrice">45.000 Ft</strong>
                                            <p class="oldPrice">79.000 Ft</p>
                                        </div>
                                    </article>
                                </a>
                                <div class="distanceBox">
                                    <span>2.8 km</span>
                                    <span class="saveOffer saved"><i class="icon icon-banner"></i></span>
                                </div>
                            </div>
                            <div class="offerItem">
                                <a href="offer_inner.php">
                                    <figure class="imgBox">
                                        <div class="offerStatusBox">
                                            <div class="statusBox">
                                                <div class="statusItem">
                                                    <img src="../assets/img/badge.svg">
                                                </div>
                                            </div>
                                            <div class="discountBox">
                                                <p>-57%</p>
                                            </div>
                                        </div>
                                        <div class="imgCaroBox">
                                            <img src="https://picsum.photos/id/160/360/220" alt="">
                                        </div>
                                    </figure>
                                    <article class="textBox">
                                        <h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
                                        <div class="offerPriceBox">
                                            <strong class="newPrice">45.000 Ft</strong>
                                            <p class="oldPrice">79.000 Ft</p>
                                        </div>
                                    </article>
                                </a>
                                <div class="distanceBox">
                                    <span>2.8 km</span>
                                    <span class="saveOffer saved"><i class="icon icon-banner"></i></span>
                                </div>
                            </div>
                            <div class="offerItem">
                                <a href="offer_inner.php">
                                    <figure class="imgBox">
                                        <div class="offerStatusBox">
                                            <div class="statusBox">
                                                <div class="statusItem">
                                                    <img src="../assets/img/badge.svg">
                                                </div>
                                            </div>
                                            <div class="discountBox">
                                                <p>-57%</p>
                                            </div>
                                        </div>
                                        <div class="imgCaroBox">
                                            <img src="https://picsum.photos/id/160/360/220" alt="">
                                        </div>
                                    </figure>
                                    <article class="textBox">
                                        <h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
                                        <div class="offerPriceBox">
                                            <strong class="newPrice">45.000 Ft</strong>
                                            <p class="oldPrice">79.000 Ft</p>
                                        </div>
                                    </article>
                                </a>
                                <div class="distanceBox">
                                    <span>2.8 km</span>
                                    <span class="saveOffer saved"><i class="icon icon-banner"></i></span>
                                </div>
                            </div>
                            <div class="offerItem">
                                <a href="offer_inner.php">
                                    <figure class="imgBox">
                                        <div class="offerStatusBox">
                                            <div class="statusBox">
                                                <div class="statusItem">
                                                    <img src="../assets/img/badge.svg">
                                                </div>
                                            </div>
                                            <div class="discountBox">
                                                <p>-57%</p>
                                            </div>
                                        </div>
                                        <div class="imgCaroBox">
                                            <img src="https://picsum.photos/id/160/360/220" alt="">
                                        </div>
                                    </figure>
                                    <article class="textBox">
                                        <h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
                                        <div class="offerPriceBox">
                                            <strong class="newPrice">45.000 Ft</strong>
                                            <p class="oldPrice">79.000 Ft</p>
                                        </div>
                                    </article>
                                </a>
                                <div class="distanceBox">
                                    <span>2.8 km</span>
                                    <span class="saveOffer saved"><i class="icon icon-banner"></i></span>
                                </div>
                            </div>
                            <div class="offerItem">
                                <a href="offer_inner.php">
                                    <figure class="imgBox">
                                        <div class="offerStatusBox">
                                            <div class="statusBox">
                                                <div class="statusItem">
                                                    <img src="../assets/img/badge.svg">
                                                </div>
                                            </div>
                                            <div class="discountBox">
                                                <p>-57%</p>
                                            </div>
                                        </div>
                                        <div class="imgCaroBox">
                                            <img src="https://picsum.photos/id/160/360/220" alt="">
                                        </div>
                                    </figure>
                                    <article class="textBox">
                                        <h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
                                        <div class="offerPriceBox">
                                            <strong class="newPrice">45.000 Ft</strong>
                                            <p class="oldPrice">79.000 Ft</p>
                                        </div>
                                    </article>
                                </a>
                                <div class="distanceBox">
                                    <span>2.8 km</span>
                                    <span class="saveOffer saved"><i class="icon icon-banner"></i></span>
                                </div>
                            </div>
                            <div class="offerItem">
                                <a href="offer_inner.php">
                                    <figure class="imgBox">
                                        <div class="offerStatusBox">
                                            <div class="statusBox">
                                                <div class="statusItem">
                                                    <img src="../assets/img/badge.svg">
                                                </div>
                                            </div>
                                            <div class="discountBox">
                                                <p>-57%</p>
                                            </div>
                                        </div>
                                        <div class="imgCaroBox">
                                            <img src="https://picsum.photos/id/160/360/220" alt="">
                                        </div>
                                    </figure>
                                    <article class="textBox">
                                        <h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
                                        <div class="offerPriceBox">
                                            <strong class="newPrice">45.000 Ft</strong>
                                            <p class="oldPrice">79.000 Ft</p>
                                        </div>
                                    </article>
                                </a>
                                <div class="distanceBox">
                                    <span>2.8 km</span>
                                    <span class="saveOffer saved"><i class="icon icon-banner"></i></span>
                                </div>
                            </div>
                        </div>
        
                    </div>
                    
                </div>

                <aside class="subPageAside right">
    
                    <div class="articlesWrapper">
    
                        <h3><a href="articles.php">Cikkajánló</a></h3>
    
                        <div class="articlesBox asideView">
                            <a href="article_inner.php" class="articleItem">
                                <h4>Free Beauty Samples What They Are And How</h4>
                                <p>There are so many different hairstyles for different people and different occasions. If you do an internet search online, you will find an amazing number of web sites and articles dedicated to tha topic…</p>
                            </a>
                            <a href="article_inner.php" class="articleItem">
                                <h4>Free Beauty Samples What They Are And How</h4>
                                <p>There are so many different hairstyles for different people and different occasions. If you do an internet search online, you will find an amazing number of web sites and articles dedicated to tha topic…</p>
                            </a>
                            <a href="article_inner.php" class="articleItem">
                                <h4>Free Beauty Samples What They Are And How</h4>
                                <p>There are so many different hairstyles for different people and different occasions. If you do an internet search online, you will find an amazing number of web sites and articles dedicated to tha topic…</p>
                            </a>
                        </div>

                        <a class="loadMoreBtn loadArticles">További cikkek</a>
    
                    </div>
                </aside>

            </div>

        </div>
    </div>

    <?php include './partials/footer.php';?>

    <?php include './partials/modals.php';?>
    
    <script src="../assets/js/plugins/jquery/jquery-3.4.1.min.js" defer></script>
    <script src="../assets/js/plugins/jquery/jquery-migrate-1.4.1.min.js" defer></script>
    <script src="../assets/js/plugins/owl-carousel/owl.carousel.min.js" defer></script>
    <script src="../assets/js/plugins/lightslider/js/lightslider.min.js" defer></script>
	<script src="../assets/js/plugins/bootstrap-modal/bootstrap-modal-min.js" defer></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
    <script src="../assets/js/plugins/lightpicker/lightpick.js" defer></script>
	<script src="../assets/js/plugins/select2/js/select2.full.min.js" defer></script>            

	<script src="../assets/js/main.js" defer></script>

<?php include './partials/Foot.php';?>