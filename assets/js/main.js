var Main = {}

Main.init = function(){
    this.menuFunctions();
    this.windowSizeListener();
    this.headerPopups();
    this.openSearchBox();
    this.usableDatePicker();
    this.userModal();
    this.citySelect();
    this.togglePasswordVisibility();
    this.headerFixing();
    this.offerCaro();
    this.subOfferCaro();
}

Main.windowSizeListener = function(){
    if($(window).width() < 767){
        $(".savedOffersBox .dropdownOpener").removeClass("dropdownOpener");
        Main.dropdownInit();
    }
    else {
        $(".savedOffersBox .dropdownOpener").removeAttr("href");
        Main.dropdownInit();
    }
}

Main.menuFunctions = function(){

    $(".menuOpen").click(function(){
        $("body").toggleClass("menuIn");
    });

}

Main.headerFixing = function(){
    if($(".pageHeader").hasClass("fixIt")){
        var currentY;
        var lastY = 0;
        var header = $(".pageHeader");
        var headerHeight = header.height();

        window.onscroll = function() {
            currentY = document.documentElement.scrollTop;
            
            if(currentY < lastY){
                if(currentY > 500){
                    header.addClass("fixPos");
                    $("body").css("padding-top", headerHeight + "px");
                }
                else {
                    header.removeClass("fixPos");
                    $("body").css("padding-top", "");
                }
            }
            lastY = currentY;
        };
    }
}

Main.headerPopups = function() {

    if($(window).width() > 767){
        setTimeout(function(){
            $(".headerPopup").fadeIn(300).css("display","flex");
        }, 1000);
        const $popup = $('.headerPopup');
        
        $(document).on("mouseup", $popup, function(e){
            if (!$popup.is(e.target) // if the target of the click isn't the container...
            && $popup.has(e.target).length === 0) // ... nor a descendant of the container
            {
                $(".headerPopup").css("display","none");
            }
        });
    }

}

Main.dropdownInit = function() {

    var parent;
    
    $("body").on("click", ".dropdownParent:not(.hoverOpen) .dropdownOpener", function(){
        parent = $(this).parents(".dropdownParent");
        parent.addClass("active");
        parent.find(".dropdown").css("display","block");

        const $dropdown = $('.mainHeader .dropdown');
    
        $(document).on("mouseup", $dropdown, function(e){
            if (!$dropdown.is(e.target) // if the target of the click isn't the container...
            && $dropdown.has(e.target).length === 0) // ... nor a descendant of the container
            {
                $(".mainHeader .dropdownParent").removeClass("active");
                $(".mainHeader .dropdown").css("display","none");
                $(document).off("mouseup");
            }
        });
    });

}

Main.navMarginSizer = function(){
    $(".mainNav > ul > li").css("margin-right","");
    if($(window).width() > 1023){
        var navWidth = $(".mainNav").width();
        var liLength = $(".mainNav > ul > li").length;
        var liSumWidth = 0;
        var marginRight = 0;

        $(".mainNav > ul > li").each(function(){
            liSumWidth += $(this).width();
        });

        marginRight = (navWidth - parseInt(liSumWidth)) / (liLength - 1);

        marginRight = parseInt(marginRight);

        $(".mainNav > ul > li:not(:last-child)").css("margin-right", marginRight);

    }
}

Main.openSearchBox = function(){
    $(".searchBoxOpener").click(function(){
        $(".headerInputsBox").toggleClass("opened");
    });
}

Main.usableDatePicker = function(){


    if($("#usableDatePicker").length > 0){

        if($("#usableDatePicker").val() !== ""){
            $(".usableDateBox .icon-cancel").css("display","inline-block");
        }
        var monthNbr = 2;
        if($(window).width() < 768){
            monthNbr = 1;
        }

        var picker = new Lightpick({ 
            field: document.getElementById('usableDatePicker'),
            lang: 'hu',
            singleDate: false,
            format: "MM.DD",
            minDate: new Date,
            numberOfMonths: monthNbr,
            /*inline: true,
            parentEl: ".calendarBox",*/
            orientation: "bottom right",
            onSelect: function(start, end){
                var str = '';
                str += start ? start.format('MM.DD') + ' - ' : '';
                str += end ? end.format('MM.DD') : '';
                document.getElementById('usableDatePicker').value = str;
                $(".usableDateBox .icon-cancel").css("display","inline-block");
            },
            onOpen: function(){
                if($(".lightpick > h3").length == 0){
                    $(".lightpick").prepend("<h3>Felhasználható</h3>");
                }
            },
            locale: {
                buttons: {
                    prev: '<i class="icon icon-previous"></i>',
                    next: '<i class="icon icon-next"></i>'
                  },
                  tooltip: {
                    one: 'nap',
                    other: 'nap'
                  },
            },
            dropdowns: false,
            hoveringTooltip: false
        });

        $(".usableDateBox .icon-cancel").click(function(){
            $("#usableDatePicker").val("");
            picker.setDateRange("", "");
            $(this).css("display","none");
        });

    }

}

Main.citySelect = function(){
    $(".locBox select").select2({
        placeholder: "Lakhely"
    });
    $(".locBox select").on('select2:select', function (e) {
        $(this).parent(".locBox").find(".icon-cancel").css("display","inline-block");
    });

    $(".locBox .icon-cancel").click(function(){
        $(this).parent(".locBox").find("select").val(null).trigger("change");
        $(this).css("display","none");
    });
}

Main.userModal = function(){

    $(".openLogin").click(function(){
        $(".userLoginModal").css("display","block");
    });
    $(".openReg").click(function(){
        $(".userRegModal").css("display","block");
    });
    $(".openForgottenPw").click(function(){
        $(".forgottenPwModal").css("display","block");
    });

    $("body").on("click",".changeModalLink",function(){
        if($(this).hasClass("toReg")){
            $(".userLoginModal, .forgottenPwModal").fadeOut(300);
            setTimeout(function(){$(".userRegModal").fadeIn(300);},300);

        }
        if($(this).hasClass("toLogin")){
            $(".userRegModal, .forgottenPwModal").fadeOut(300);
            setTimeout(function(){$(".userLoginModal").fadeIn(300);},300);
        }

        if($(this).hasClass("toFPW")){
            $(".userRegModal, .userLoginModal").fadeOut(300);
            setTimeout(function(){$(".forgottenPwModal").fadeIn(300);},300);
        }
    });

    $("#userModal").on("hidden.bs.modal",function(){
        $(".userLoginModal, .userRegModal, .forgottenPwModal").css("display","none");
    });
    
}

Main.togglePasswordVisibility = function(){
    $("body").on("click", ".pwVisibilityToggler", function(){
        var icon = $(this);
        var input = icon.parent(".inputBox").find("input");
        if (input.attr("type") === "password") {
            input.attr("type","text");
            icon.addClass("showIt");
        } else {
            input.attr("type","password");
            icon.removeClass("showIt");
        }
    });
}

Main.subOfferCaro = function(){
    if($(".subOffers").length > 0){
        if($(".subOffers").parent(".subPageAside").length > 0 ){
            console.info("1");
            var subOffersCaro = $(".subOffers .offersBox").owlCarousel({
                loop: false,
                items: 1,
                autoplay: true,
                margin: 30,
                dots: false,
                nav: false,
                responsive : {
                    300 : {
                        items: 1,
                        loop: false,
                        margin: 20
                    },
                    500 : {
                        items: 2,
                        loop: false,
                        margin: 20
                    },
                    768 : {
                        items: 2,
                        loop: false
                    },
                    1024 : {
                        items: 3,
                        loop: false
                    },
                    1200: {
                        items: 1,
                        loop: false
                    }
                }
            });
        }
        else {
            var subOffersCaro = $(".subOffers .offersBox").owlCarousel({
                loop: false,
                items: 4,
                autoplay: true,
                margin: 30,
                dots: false,
                nav: false,
                responsive : {
                    300 : {
                        items: 1,
                        loop: false,
                        margin: 20
                    },
                    500 : {
                        items: 2,
                        loop: false,
                        margin: 20
                    },
                    768 : {
                        items: 2,
                        loop: false
                    },
                    1024 : {
                        items: 3,
                        loop: false
                    },
                    1200: {
                        items: 4,
                        loop: false
                    }
                }
            });
        }
    }
}

Main.offerCaro = function(){
    if($(".offerItem .imgCaroBox").length > 0){
        $(".offerItem .imgCaroBox").each(function(){
            if($(this).find("img").length > 1){
                $(this).lightSlider({
                    loop: true,
                    items: 1,
                    mode: "fade",
                    auto: true,
                    //controls: true,
                    pager: true,
                    pauseOnHover: true,
                    pause: 5000
                });
            }
        });
        
    }
}

$(document).ready(function(){
    Main.init();
});

$(window).load(function(){
    Main.navMarginSizer();
});

$(window).resize(function(){
    Main.navMarginSizer();
});