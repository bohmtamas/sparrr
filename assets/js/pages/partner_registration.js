var Partner_Registration = {}
 Partner_Registration.init = function(){

    this.toggleAddressVisibility();
}

Partner_Registration.toggleAddressVisibility = function(){
    $("body").on("change", "#addressDiffChb", function(){
        if($(this).is(":checked")){
            $(".inputRow.billingAddress").css("display","flex");
        }
        else {
            $(".inputRow.billingAddress").css("display","none");
        }
    });
}

$(document).ready(function(){
    Partner_Registration.init();
});