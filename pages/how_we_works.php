<?php include './partials/Head.php';?>

	<?php include './partials/header.php';?>

    <div class="pageContent subPageContent">
        <div class="container">

            <h1 class="subPageTitle">Hogyan működünk?</h1>

            <div class="subPageMain center">

                <article class="mainTextBox">
            
                    <p>Fontos számunkra, hogy jól érezze magát és ne érje meglepetés, így a Gutschein-ok tartalmazzák a legfontosabb feltételeket:</p>
            
                    <br><br>

                    <ul>
                        <li>Érvényességi idő – meddig érvényes az ajánlat és vannak-e olyan időszakok amikor nem vagy csak felárral</li>
                        <li>Milyen szolgáltatásra vonatkozik a Gutschein – amennyiben mást is szeretne igénybe venni, azt közvetlenül a Partner céggel tudja rendezni</li>
                        <li>Foglalási feltételek – a leírásban megtalálja, hogy mennyi idővel a felhasználás előtt kezdje meg foglalását</li>
                        <li>Elérhetőségek – hol és kinél érdeklődhet szabad helyek iránt</li>
                        <li>Lemondási és módosítási feltételek – nem tud mégsem elmenni? A feltételek között megtalálja, hogy  mikor és hogyan tudja módosítani vagy lemondani. Ha ezt nem teszi meg időben, a Gutschein-ja beváltottnak tekinthető</li>
                        <li>Elállás – 14 nappal a a vásárlást követően elállhat a vásárlástól. Kérem jelezze felénk írásban az alábbi címen: info@sparenimurlaub.de. Ez alól kivétel a fix dátumra megvett programok (pl.: koncertjegy).</li>
                    </ul>

                    <br><br>

                    <p>Felhasználáskor vigye magával a kinyomtatott Gutschein-t vagy mutassa be mobilján: az egyedi kód alapján tudja ellenőrizni a Partnerünk, hogy érvényes-e a Gutschein-ja.</p>

                    <br><br>

                    <p>Minden Gutschein-t csak egyszer tud felhasználni!</p>

                    <p>További feltételeket megnézheti az <a>ÁSZF</a>-ben.</p>

                    <br><br>

                    <p>Ha kérdése lenne, kérjük írjon nekünk az Ügyfélszolgálat részben!</p>

                    <br><br>

                    <p>Üdvözlettel,<br>Sparen im Urlaub csapata</p>
            
                </article>
            </div>

        </div>
    </div>

    <?php include './partials/footer.php';?>

	<?php include './partials/modals.php';?>

    <script src="../assets/js/plugins/jquery/jquery-3.4.1.min.js" defer></script>
    <script src="../assets/js/plugins/jquery/jquery-migrate-1.4.1.min.js" defer></script>
    <script src="../assets/js/plugins/owl-carousel/owl.carousel.min.js" defer></script>
    <script src="../assets/js/plugins/lightslider/js/lightslider.min.js" defer></script>
	<script src="../assets/js/plugins/bootstrap-modal/bootstrap-modal-min.js" defer></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
	<script src="../assets/js/plugins/lightpicker/lightpick.js" defer></script>
	<script src="../assets/js/plugins/select2/js/select2.full.min.js" defer></script>        
    <script src="../assets/js/plugins/lightgallery/js/lightgallery.min.js" defer></script>                

	<script src="../assets/js/main.js" defer></script>
	<script src="../assets/js/pages/content.js" defer></script>

<?php include './partials/Foot.php';?>