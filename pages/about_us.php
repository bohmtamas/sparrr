<?php include './partials/Head.php';?>

	<?php include './partials/header.php';?>

    <div class="pageContent subPageContent">
        <div class="container">

            <h1 class="subPageTitle">Rólunk</h1>
            <h2 class="subPageSubTitle">Da bekommst du mehr für dein Geld!</h2>

            <div class="subPageMain center">

                <article class="mainTextBox">
            
                    <p>Szeretünk utazni. Sokat! Nincs is ennél jobb dolog a világon! Még le sem telt, de már szervezzük a következőt :)!</p>
            
                    <br><br>

                    <p>Egy utazás sok mindent magában foglalhat! A pihenésen, városnézésen vagy shoppingoláson kívül rengeteg más dologra van még lehetőség: fogászat, gyógykezelések, fodrászat, testkezelések. Nálunk mindent együtt megtalál, így könnyen és gyorsan megszervezheti több napos programját. Fontos számunkra, hogy ne érje meglepetés (legalábbis ne kellemetlen:-)) így munkatársaink a legjobb helyek ajánlataiból válogattak! Kiemelkedően jó élmények – kellemes árakon! Gute Spart!</p>

                </article>

                <div class="ourTeam">

                    <div class="teamMember">
                        <figure class="imgBox">
                            <img src="https://picsum.photos/id/155/105/105" alt="">
                        </figure>
                        <h6>Delia Garcia</h6>
                        <span>titulus</span>
                        <a href="mailto:delia.garcia@sparrrinungar.com">delia.garcia@sparrrinungar.com</a>
                        <p>Maecenas lacinia, arcu sit amet laoreet efficitur, mauris lectus porttitor nisi, eget viverra sem tellus. </p>
                    </div>
                    <div class="teamMember">
                        <figure class="imgBox">
                            <img src="https://picsum.photos/id/155/105/105" alt="">
                        </figure>
                        <h6>Delia Garcia</h6>
                        <span>titulus</span>
                        <a href="mailto:delia.garcia@sparrrinungar.com">delia.garcia@sparrrinungar.com</a>
                        <p>Maecenas lacinia, arcu sit amet laoreet efficitur, mauris lectus porttitor nisi, eget viverra sem tellus. </p>
                    </div>
                    <div class="teamMember">
                        <figure class="imgBox">
                            <img src="https://picsum.photos/id/155/105/105" alt="">
                        </figure>
                        <h6>Delia Garcia</h6>
                        <span>titulus</span>
                        <a href="mailto:delia.garcia@sparrrinungar.com">delia.garcia@sparrrinungar.com</a>
                        <p>Maecenas lacinia, arcu sit amet laoreet efficitur, mauris lectus porttitor nisi, eget viverra sem tellus. </p>
                    </div>
                
                </div>

            </div>

        </div>
    </div>

    <?php include './partials/footer.php';?>

    <?php include './partials/modals.php';?>
		

    <script src="../assets/js/plugins/jquery/jquery-3.4.1.min.js" defer></script>
    <script src="../assets/js/plugins/jquery/jquery-migrate-1.4.1.min.js" defer></script>
    <script src="../assets/js/plugins/owl-carousel/owl.carousel.min.js" defer></script>
    <script src="../assets/js/plugins/lightslider/js/lightslider.min.js" defer></script>
	<script src="../assets/js/plugins/bootstrap-modal/bootstrap-modal-min.js" defer></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
	<script src="../assets/js/plugins/lightpicker/lightpick.js" defer></script>
	<script src="../assets/js/plugins/select2/js/select2.full.min.js" defer></script>        
    <script src="../assets/js/plugins/lightgallery/js/lightgallery.min.js" defer></script>                

	<script src="../assets/js/main.js" defer></script>
	<script src="../assets/js/pages/content.js" defer></script>

<?php include './partials/Foot.php';?>