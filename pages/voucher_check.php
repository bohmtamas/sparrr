<?php include './partials/Head.php';?>

	<?php include './partials/partnerHeader.php';?>

    <div class="pageContent subPageContent partnerPageContent">
        <div class="container">

            <h1 class="subPageTitle">Voucher ellenőrzés</h1>

            <div class="subPageMain full flex">

				<div class="voucherCheckBox firstStep">
                    <p class="checkInfo">Kérem adja meg a Gutschein kódját az érvényesség ellenőrzéséhez!</p>
                    <p class="successMsg">Sikeres beváltás! Lorem ipsum dolor sit amet!</p>
                    <form id="voucherCheckForm">
                        <div class="inputBox">
                            <label>Voucher kód</label>
                            <input name="voucher_code" type="text">
                            <i class="icon icon-check"></i>
                            <i class="icon icon-cancel"></i>
                        </div>
                        <button type="button" class="btn greenBtn rounded checkBtn">ellenőrzés</button>
                        <button type="button" class="btn greyBtn rounded voucherBackBtn">visszalépés</button>
                        <button type="button" class="btn greenBtn rounded disabled confirmBtn">beváltás</button>
                    </form>
                </div>

            </div>

        </div>
    </div>

    <?php include './partials/footer.php';?>

	<?php include './partials/modals.php';?> 
		
    <script src="../assets/js/plugins/jquery/jquery-3.4.1.min.js" defer></script>
    <script src="../assets/js/plugins/jquery/jquery-migrate-1.4.1.min.js" defer></script>
    <script src="../assets/js/plugins/owl-carousel/owl.carousel.min.js" defer></script>
	<script src="../assets/js/plugins/bootstrap-modal/bootstrap-modal-min.js" defer></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
	<script src="../assets/js/plugins/lightpicker/lightpick.js" defer></script>
	<script src="../assets/js/plugins/select2/js/select2.full.min.js" defer></script>        

	<script src="../assets/js/main.js" defer></script>
	<script src="../assets/js/pages/voucher_check.js" defer></script>

<?php include './partials/Foot.php';?>