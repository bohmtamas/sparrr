<?php include './partials/Head.php';?>

	<?php include './partials/header.php';?>

    <div class="pageContent subPageContent">
        <div class="container">

            <h1 class="subPageTitle">Vásárlásaim</h1>

            <nav class="profilePageNav">
                <ul>
					<li><a href="profile.php">Személyes adatok</a></li>
                    <li class="active"><a href="vouchers.php">Vásárlásaim</a></li>
                    <!--<li><a href="balance.php">Egyenlegem</a></li>-->
                </ul>
            </nav>

            <div class="subPageMain full flex">

				<div class="vouchersWrapper">
					<h5>Aktív kuponjaim</h5>
					<div class="vouchersBox">
						<table class="vouchersTable">
							<thead>
								<th>Kupon</th>
								<th>Érvényességi idő</th>
								<th>Ár</th>
								<th>Mennyiség</th>
								<th>Összeg</th>
							</thead>
							<tbody>
								<tr>
									<td>
										<a class="offerItem">
											<figure class="imgBox">
												<div class="offerStatusBox">
													<div class="statusBox"></div>
													<div class="discountBox">
														<p>-57%</p>
													</div>
												</div>
												<img src="https://picsum.photos/id/155/360/220" alt="">
											</figure>
											<article class="textBox">
												<h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
												<ul class="tableMobileInfo">
													<li>
														<span>Érvényességi idő:</span>
														<span>2019.05.12 - 2020.02.02.</span>
													</li>
													<li>
														<span>Ár:</span>
														<span>29.900 Ft</span>
													</li>
													<li>
														<span>Mennyiség:</span>
														<span>1 db</span>
													</li>
													<li>
														<span>Összeg:</span>
														<span>29.900 Ft</span>
													</li>
												</ul>
											</article>
										</a>
									</td>
									<td class="hideOnTablet">2019.05.12 - 2020.02.02.</td>
									<td class="hideOnTablet">29.900 Ft</td>
									<td class="hideOnTablet">1 db</td>
									<td class="hideOnTablet">29.900 Ft</td>
								</tr>
								<tr>
									<td>
										<a class="offerItem">
											<figure class="imgBox">
												<div class="offerStatusBox">
													<div class="statusBox"></div>
													<div class="discountBox">
														<p>-57%</p>
													</div>
												</div>
												<img src="https://picsum.photos/id/155/360/220" alt="">
											</figure>
											<article class="textBox">
												<h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
												<ul class="tableMobileInfo">
													<li>
														<span>Érvényességi idő:</span>
														<span>2019.05.12 - 2020.02.02.</span>
													</li>
													<li>
														<span>Ár:</span>
														<span>29.900 Ft</span>
													</li>
													<li>
														<span>Mennyiség:</span>
														<span>1 db</span>
													</li>
													<li>
														<span>Összeg:</span>
														<span>29.900 Ft</span>
													</li>
												</ul>
											</article>
										</a>
									</td>
									<td class="hideOnTablet">2019.05.12 - 2020.02.02.</td>
									<td class="hideOnTablet">29.900 Ft</td>
									<td class="hideOnTablet">1 db</td>
									<td class="hideOnTablet">29.900 Ft</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>

            </div>

        </div>
    </div>

    <?php include './partials/footer.php';?>

	<?php include './partials/modals.php';?> 
		
    <script src="../assets/js/plugins/jquery/jquery-3.4.1.min.js" defer></script>
    <script src="../assets/js/plugins/jquery/jquery-migrate-1.4.1.min.js" defer></script>
    <script src="../assets/js/plugins/owl-carousel/owl.carousel.min.js" defer></script>
	<script src="../assets/js/plugins/bootstrap-modal/bootstrap-modal-min.js" defer></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
	<script src="../assets/js/plugins/lightpicker/lightpick.js" defer></script>
	<script src="../assets/js/plugins/select2/js/select2.full.min.js" defer></script>        

	<script src="../assets/js/main.js" defer></script>

<?php include './partials/Foot.php';?>