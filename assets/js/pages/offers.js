var Offers = {};

Offers.init = function(){
    this.mainCaro();
    this.mainOffersCaro();
    this.offerTypeSelect();
    this.offerOrderSelect();
}

Offers.mainCaro = function (){
    var mainCaro = $("#mainCaro").owlCarousel({
        loop: true,
        items: 1,
        animateOut: "fadeOut",
        autoplay: true
    });
}

Offers.mainOffersCaro = function (){
    var mainOffersCaro = $(".mainOffersSection .mainOffersBox").owlCarousel({
        loop: false,
        items: 2,
        autoplay: false,
        margin: 30,
        dots: false,
        mouseDrag: false,
        responsive : {
            300 : {
                items: 1,
                margin: 20,
                mouseDrag: true
            },
            500 : {
                items: 2,
                margin: 20
            },
            768 : {
                margin: 30,
                loop: false
            }
        }
    });
}

Offers.offerTypeSelect = function (){
    $("#offerTypeSelect").select2({
        minimumResultsForSearch: -1
    });
}

Offers.offerOrderSelect = function (){
    $("#offerOrderSelect").select2({
        minimumResultsForSearch: -1
    });
}

$(document).ready(function(){
    Offers.init();
});