var Hello_Nachbarn = {};

Hello_Nachbarn.init = function(){
    this.mainCaro();
    this.mainOffersCaro();
    this.offerOrderSelect();
    this.citySelect();
}

Hello_Nachbarn.mainCaro = function (){
    var mainCaro = $("#mainCaro").owlCarousel({
        loop: true,
        items: 1,
        animateOut: "fadeOut",
        autoplay: true
    });
}

Hello_Nachbarn.mainOffersCaro = function (){
    var mainOffersCaro = $(".mainOffersSection .mainOffersBox").owlCarousel({
        loop: false,
        items: 2,
        autoplay: true,
        margin: 30,
        dots: false,
        responsive : {
            300 : {
                items: 1,
                loop: false,
                margin: 20
            },
            500 : {
                items: 2,
                loop: false,
                margin: 20
            },
            768 : {
                items: 2,
                loop: false
            },
            1200: {
                items: 2,
                loop: false
            }
        }
    });
}

Hello_Nachbarn.citySelect = function (){
    $(".liveLocBox select").select2({
        placeholder: 'Lakóhelyem, pl.: Wien'
    });
    $(".liveLocBox select").on('select2:select', function (e) {
        $(this).parent(".liveLocBox").find(".icon-cancel").css("display","inline-block");
    });

    $(".liveLocBox .icon-cancel").click(function(){
        $(this).parent(".liveLocBox").find("select").val(null).trigger("change");
        $(this).css("display","none");
    });
}

Hello_Nachbarn.offerOrderSelect = function (){
    $("#offerOrderSelect").select2({
        minimumResultsForSearch: -1
    });
}

$(document).ready(function(){
    Hello_Nachbarn.init();
});