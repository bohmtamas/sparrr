const autoprefixer = require("autoprefixer");
const cssnano = require("cssnano");
const eslint = require("gulp-eslint");
const gulp = require("gulp");
const imagemin = require("gulp-imagemin");
const newer = require("gulp-newer");
const plumber = require("gulp-plumber");
const postcss = require("gulp-postcss");
const rename = require("gulp-rename");
const sass = require("gulp-sass");
const iconfont = require('gulp-iconfont');
const async = require('async');
const consolidate = require('gulp-consolidate');

// Optimize Images
function images() {
  return gulp
    .src("./assets/img/**/*")
    .pipe(newer("./assets/img"))
    .pipe(
      imagemin([
        imagemin.gifsicle({ interlaced: true }),
        imagemin.jpegtran({ progressive: true }),
        imagemin.optipng({ optimizationLevel: 5 }),
        imagemin.svgo({
          plugins: [
            {
              removeViewBox: false,
              collapseGroups: true
            }
          ]
        })
      ])
    )
    .pipe(gulp.dest("./assets/img"));
}

// Iconfont
gulp.task('Iconfont', function(done){
  var iconStream = gulp.src(['assets/icons/*.svg'])
    .pipe(iconfont({ fontName: 'myfont', normalize: true, fontHeight: 1001, formats: ['ttf', 'eot', 'woff', 'woff2', 'svg'], }));

  async.parallel([
    function handleGlyphs (cb) {
      iconStream.on('glyphs', function(glyphs, options) {
        gulp.src('assets/scss/icons/_template.scss')
          .pipe(consolidate('lodash', {
            glyphs: glyphs,
            fontName: 'myfont',
            fontPath: '../fonts/',
            className: 'icon'
          }))
          .pipe(rename("icons.scss"))
          .pipe(gulp.dest('./assets/scss/icons'))
          .on('finish', cb);
      });
    },
    function handleFonts (cb) {
      iconStream
        .pipe(gulp.dest('assets/fonts/'))
        .on('finish', cb);
    }
  ], done);
});

// CSS task
function css() {
  return gulp
    .src("./assets/scss/**/*.scss")
    .pipe(plumber())
    .pipe(sass({ outputStyle: "expanded" }))
    .pipe(gulp.dest("./assets/css/"))
    .pipe(rename({ suffix: ".min" }))
    .pipe(postcss([autoprefixer(), cssnano()]))
    .pipe(gulp.dest("./assets/css/"));
}

// Lint scripts
function scriptsLint() {
  return gulp
    .src(["./assets/js/**/*", "./gulpfile.js"])
    .pipe(plumber())
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(eslint.failAfterError());
}

// Watch files
function watchFiles() {
    gulp.watch("./assets/scss/**/*", css);
    gulp.watch("./assets/js/**/*", scriptsLint);
    gulp.watch("./assets/img/*", images);
}

// define complex tasks
const watch = gulp.parallel(images, css, watchFiles);

// export tasks
exports.images = images;
exports.css = css;

exports.watch = watch;
exports.default = watch;