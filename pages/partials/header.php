<header class="pageHeader fixIt">
    <div class="mainHeader">
        <div class="container clearfix">
            <a href="/pages" class="mainLogo">
                <img class="desktopLogo" src="../assets/img/logo.svg" alt="Sparen im Urlaub logo">
                <img class="mobileLogo" src="../assets/img/logo-mobile.svg" alt="Sparen im Urlaub logo">
            </a>
            <button class="menuOpen">
                <span></span>
            </button>
            <a href="country-select.php" class="countrySelector" title="Országválasztó"><i class="icon icon-globe"></i></a>
            <div class="menuBox">
                <header>
                    <a href="/pages"><img class="mobileLogo" src="../assets/img/logo-mobile-white.svg" alt="Sparen im Urlaub logo"></a>
                </header>
                <!-- HA BE VAN JELENTKEZVE -->
                <div class="itsMeBox">
                    <h4>Kertész Márk</h4>
                    <p>mark.kertesz01@gmail.com</p>
                </div>
                <nav class="mobileMenu">
                    <ul>
                        <li><a href="profile.php">Profilom<i class="icon icon-arrowRight"></i></a></li>
                    </ul>
                </nav>
                <!-- HA NINCS BEJELENTKEZVE -->
                <nav class="mobileMenu">
                    <ul>
                        <li><a href="login.php">Bejelentkezés<i class="icon icon-arrowRight"></i></a></li>
                        <li><a href="registration.php">Regisztráció<i class="icon icon-arrowRight"></i></a></li>
                    </ul>
                </nav>
                <nav class="mobileMenu">
                    <ul>
                        <li><a href="about_us.php">Rólunk<i class="icon icon-arrowRight"></i></a></li>
                        <li><a href="how_we_work.php">Hogyan működünk?<i class="icon icon-arrowRight"></i></a></li>
                        <li><a href="contact.php">Kapcsolat<i class="icon icon-arrowRight"></i></a></li>
                    </ul>
                </nav>
                <nav class="mobileMenu">
                    <ul>
                        <li><a>Kijelentkezés<i class="icon icon-arrowRight"></i></a></li>
                    </ul>
                </nav>
            </div>
            <div class="headerActions">
                <div class="headerInputsBox">
                    <div class="inputBox searchBox">
                        <i class="icon icon-search"></i>
                        <input type="text" placeholder="Keresés">
                    </div>
                    <div class="inputBox locBox">
                        <i class="icon icon-pin"></i>
                        <select>
                            <option></option>
                            <option value="Budapest">Budapest</option>
                            <option value="Pécs">Pécs</option>
                            <option value="Szeged">Szeged</option>
                        </select>
                        <i class="icon icon-cancel"></i>
                        <div class="cityPopup headerPopup">
                            <p>Tudja már melyik város?</p>
                            <img src="../assets/img/arrow-popup-right.svg" alt="arrow right">
                        </div>
                    </div>
                    <div class="inputBox usableDateBox ">
                        <i class="icon icon-calendar"></i>
                        <input type="text" placeholder="Felhasználható" readonly id="usableDatePicker">
                        <i class="icon icon-cancel"></i>
                        <div class="datePopup headerPopup">
                            <img src="../assets/img/arrow-popup-left.svg" alt="arrow left">
                            <p>Tudja már mikor utazik?</p>
                        </div>
                    </div>
                </div>
                <div class="headSearchBox">
                    <a class="searchBoxOpener"><i class="icon icon-search"></i></a>
                </div>
                <div class="savedOffersBox dropdownParent"> <!-- HA NINCS BEJEJELENTKEZVE, AKKOR "dropdownParent" OSZTÁLY NEM KELL -->
                    <!-- HA BE VAN JELENTKEZVE ELEJE -->
                    <a href="saved_offers.php" class="dropdownOpener">
                        <i class="icon icon-banner"></i>
                        <span class="badge">3</span>
                    </a>
                    <div class="savedOffersDropdown dropdown">
                        <h3>Mentett ajánlataim</h3>
                        <ul class="savedOffersList">
                            <li>
                                <a href="offer_inner.php" class="offerItem">
                                    <figure class="imgBox">
                                        <div class="offerStatusBox">
                                            <div class="statusBox"></div>
                                            <div class="discountBox">
                                                <p>-57%</p>
                                            </div>
                                        </div>
                                        <img src="https://picsum.photos/id/155/360/220" alt="">
                                    </figure>
                                    <article class="textBox">
                                        <h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
                                        <div class="offerPriceBox">
                                            <strong class="newPrice">45.000 Ft</strong>
                                            <p class="oldPrice">79.000 Ft</p>
                                        </div>
                                    </article>
                                </a>
                                <a class="removeSavedOffer"><i class="icon icon-close"></i></a>
                            </li>
                            <li>
                                <a href="offer_inner.php" class="offerItem">
                                    <figure class="imgBox">
                                        <div class="offerStatusBox">
                                            <div class="statusBox"></div>
                                            <div class="discountBox">
                                                <p>-57%</p>
                                            </div>
                                        </div>
                                        <img src="https://picsum.photos/id/155/360/220" alt="">
                                    </figure>
                                    <article class="textBox">
                                        <h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
                                        <div class="offerPriceBox">
                                            <strong class="newPrice">45.000 Ft</strong>
                                            <p class="oldPrice">79.000 Ft</p>
                                        </div>
                                    </article>
                                </a>
                                <a class="removeSavedOffer"><i class="icon icon-close"></i></a>
                            </li>
                            <li>
                                <a href="offer_inner.php" class="offerItem">
                                    <figure class="imgBox">
                                        <div class="offerStatusBox">
                                            <div class="statusBox"></div>
                                            <div class="discountBox">
                                                <p>-57%</p>
                                            </div>
                                        </div>
                                        <img src="https://picsum.photos/id/155/360/220" alt="">
                                    </figure>
                                    <article class="textBox">
                                        <h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
                                        <div class="offerPriceBox">
                                            <strong class="newPrice">45.000 Ft</strong>
                                            <p class="oldPrice">79.000 Ft</p>
                                        </div>
                                    </article>
                                </a>
                                <a class="removeSavedOffer"><i class="icon icon-close"></i></a>

                            </li>
                        </ul>
                        <a href="saved_offers.php" class="toAllSavedOffer">Összes (+<span>4</span>)</a>
                    </div>
                    <!-- HA BE VAN JELENTKEZVE VÉGE -->
                    
                    <!-- HA NINCS BEJELENTKEZVE ELEJE 
                    <a data-toggle="modal" href="#userModal" class="openLogin">
                        <i class="icon icon-banner"></i>
                    </a>
                    HA NINCS BEJELENTKEZVE VÉGE -->

                </div>
                <div class="loginBox dropdownParent">

                    <!-- HA NINCS BEJELENTKEZVE
                    <a href="#userModal" data-toggle="modal" class="loginLink openLogin ">Bejelentkezés</a>
                    -->
                    
                    <!-- HA BE VAN JELENTKEZVE -->
                    <a class="dropdownOpener"><i class="icon icon-profile"></i></a>
                    <div class="profileDropdown dropdown">
                        <div class="itsMeBox">
                            <h4>Kertész Márk</h4>
                            <p>mark.kertesz01@gmail.com</p>
                        </div>
                        <ul class="profileMenu">
                            <li><a href="profile.php">Személyes adatok</a></li>
                            <li><a href="vouchers.php">Voucherek</a></li>
                            <li class="logoutLink"><a>Kijelentkezés</a></li>
                        </ul>
                    </div>
                    
                </div>
                <div class="toTheCartBox">
                    <a href="basket.php">
                        <i class="icon icon-cart"></i>
                        <span class="badge">2</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="mainNavBox">
        <div class="container">
            <nav class="mainNav">
                <ul class="clearfix">
                    <li>
                        <a href="offers.php">Összes ajánlat</a>
                    </li>
                    <li class="dropdownParent hoverOpen">
                        <a href="offers.php" class="dropdownOpener">Szállás</a>
                        <div class="subCatDropdown dropdown">
                            <div>
                                <nav class="subCatMenu">
                                    <ul>
                                        <li><a href="subcategory.php">Balaton</a></li>
                                        <li><a href="subcategory.php">Wellnes & Spa</a></li>
                                        <li><a href="subcategory.php">Városlátogatás</a></li>
                                        <li><a href="subcategory.php">Gyógyvíz, kezelések</a></li>
                                        <li><a href="subcategory.php">Kemping, Apartman</a></li>
                                        <li><a href="subcategory.php">Családi</a></li>
                                        <li><a href="subcategory.php">Romantikus</a></li>
                                        <li><a href="subcategory.php">Prémium</a></li>
                                        <li><a href="subcategory.php">Akadálymentesített</a></li>
                                        <li><a href="subcategory.php">Egyéb</a></li>
                                    </ul>
                                </nav>
                            </div>
                            <div>
                                <div class="offersBox">
                                    <a href="offer_inner.php" class="offerItem">
                                        <figure class="imgBox">
                                            <div class="offerStatusBox">
                                                <div class="statusBox"></div>
                                                <div class="discountBox">
                                                    <p>-57%</p>
                                                </div>
                                            </div>
                                            <div class="imgCaroBox owl-carousel">
                                                <img src="https://picsum.photos/id/162/360/220" alt="">
                                            </div>
                                        </figure>
                                        <article class="textBox">
                                            <h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
                                        </article>
                                    </a>
                                    <a href="offer_inner.php" class="offerItem">
                                        
                                        <figure class="imgBox">
                                            <div class="offerStatusBox">
                                                <div class="statusBox"></div>
                                                <div class="discountBox">
                                                    <p>-57%</p>
                                                </div>
                                            </div>
                                            <div class="imgCaroBox owl-carousel">
                                                <img src="https://picsum.photos/id/163/360/220" alt="">
                                            </div>
                                        </figure>
                                        <article class="textBox">
                                            <h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
                                        </article>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="dropdownParent hoverOpen">
                        <a href="offers.php" class="dropdownOpener">Egészség</a>
                        <div class="subCatDropdown dropdown">
                            <div>
                                <nav class="subCatMenu">
                                    <ul>
                                        <li><a href="subcategory.php">Fogászat</a></li>
                                        <li><a href="subcategory.php">Plasztika</a></li>
                                        <li><a href="subcategory.php">Egynapos beavatkozás</a></li>
                                        <li><a href="subcategory.php">Gyógyvíz, kezeléssel</a></li>
                                        <li><a href="subcategory.php">Optika</a></li>
                                        <li><a href="subcategory.php">Masszázs</a></li>
                                        <li><a href="subcategory.php">Egyéb</a></li>
                                    </ul>
                                </nav>
                            </div>
                            <div>
                                <div class="offersBox">
                                    <a href="offer_inner.php" class="offerItem">
                                        <figure class="imgBox">
                                            <div class="offerStatusBox">
                                                <div class="statusBox"></div>
                                                <div class="discountBox">
                                                    <p>-57%</p>
                                                </div>
                                            </div>
                                            <div class="imgCaroBox owl-carousel">
                                                <img src="https://picsum.photos/id/162/360/220" alt="">
                                            </div>
                                        </figure>
                                        <article class="textBox">
                                            <h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
                                        </article>
                                    </a>
                                    <a href="offer_inner.php" class="offerItem">
                                        
                                        <figure class="imgBox">
                                            <div class="offerStatusBox">
                                                <div class="statusBox"></div>
                                                <div class="discountBox">
                                                    <p>-57%</p>
                                                </div>
                                            </div>
                                            <div class="imgCaroBox owl-carousel">
                                                <img src="https://picsum.photos/id/163/360/220" alt="">
                                            </div>
                                        </figure>
                                        <article class="textBox">
                                            <h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
                                        </article>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="dropdownParent hoverOpen">
                        <a href="offers.php" class="dropdownOpener">Élmény</a>
                        <div class="subCatDropdown dropdown">
                            <div>
                                <nav class="subCatMenu">
                                    <ul>
                                        <li><a href="subcategory.php">Art</a></li>
                                        <li><a href="subcategory.php">Gyerek</a></li>
                                        <li><a href="subcategory.php">Adrenalin</a></li>
                                        <li><a href="subcategory.php">Szabadulószoba</a></li>
                                        <li><a href="subcategory.php">Városnézés</a></li>
                                        <li><a href="subcategory.php">Fürdő, gyógy-</a></li>
                                        <li><a href="subcategory.php">Events</a></li>
                                        <li><a href="subcategory.php">Gasztro</a></li>
                                        <li><a href="subcategory.php">Shopping</a></li>
                                        <li><a href="subcategory.php">Nightlife</a></li>
                                        <li><a href="subcategory.php">Egyéb</a></li>
                                    </ul>
                                </nav>
                            </div>
                            <div>
                                <div class="offersBox">
                                    <a href="offer_inner.php" class="offerItem">
                                        <figure class="imgBox">
                                            <div class="offerStatusBox">
                                                <div class="statusBox"></div>
                                                <div class="discountBox">
                                                    <p>-57%</p>
                                                </div>
                                            </div>
                                            <div class="imgCaroBox owl-carousel">
                                                <img src="https://picsum.photos/id/162/360/220" alt="">
                                            </div>
                                        </figure>
                                        <article class="textBox">
                                            <h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
                                        </article>
                                    </a>
                                    <a href="offer_inner.php" class="offerItem">
                                        
                                        <figure class="imgBox">
                                            <div class="offerStatusBox">
                                                <div class="statusBox"></div>
                                                <div class="discountBox">
                                                    <p>-57%</p>
                                                </div>
                                            </div>
                                            <div class="imgCaroBox owl-carousel">
                                                <img src="https://picsum.photos/id/163/360/220" alt="">
                                            </div>
                                        </figure>
                                        <article class="textBox">
                                            <h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
                                        </article>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="dropdownParent hoverOpen">
                        <a href="offers.php" class="dropdownOpener">Gasztro</a>
                        <div class="subCatDropdown dropdown">
                            <div>
                                <nav class="subCatMenu">
                                    <ul>
                                        <li><a href="subcategory.php">Magyaros</a></li>
                                        <li><a href="subcategory.php">Ruin-bar</a></li>
                                        <li><a href="subcategory.php">Fine dining</a></li>
                                        <li><a href="subcategory.php">Michelin</a></li>
                                        <li><a href="subcategory.php">Street food</a></li>
                                        <li><a href="subcategory.php">Lakásétterem</a></li>
                                        <li><a href="subcategory.php">Cukrászda, kávézó</a></li>
                                        <li><a href="subcategory.php">Bor</a></li>
                                        <li><a href="subcategory.php">Egyéb</a></li>
                                    </ul>
                                </nav>
                            </div>
                            <div>
                                <div class="offersBox">
                                    <a href="offer_inner.php" class="offerItem">
                                        <figure class="imgBox">
                                            <div class="offerStatusBox">
                                                <div class="statusBox"></div>
                                                <div class="discountBox">
                                                    <p>-57%</p>
                                                </div>
                                            </div>
                                            <div class="imgCaroBox owl-carousel">
                                                <img src="https://picsum.photos/id/162/360/220" alt="">
                                            </div>
                                        </figure>
                                        <article class="textBox">
                                            <h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
                                        </article>
                                    </a>
                                    <a href="offer_inner.php" class="offerItem">
                                        
                                        <figure class="imgBox">
                                            <div class="offerStatusBox">
                                                <div class="statusBox"></div>
                                                <div class="discountBox">
                                                    <p>-57%</p>
                                                </div>
                                            </div>
                                            <div class="imgCaroBox owl-carousel">
                                                <img src="https://picsum.photos/id/163/360/220" alt="">
                                            </div>
                                        </figure>
                                        <article class="textBox">
                                            <h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
                                        </article>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="dropdownParent hoverOpen">
                        <a href="offers.php" class="dropdownOpener">Szépség</a>
                        <div class="subCatDropdown dropdown">
                            <div>
                                <nav class="subCatMenu">
                                    <ul>
                                        <li><a href="subcategory.php">Kozmetika</a></li>
                                        <li><a href="subcategory.php">Fodrász</a></li>
                                        <li><a href="subcategory.php">Sminktetoválás</a></li>
                                        <li><a href="subcategory.php">Testkezelés</a></li>
                                        <li><a href="subcategory.php">Arckezelés</a></li>
                                        <li><a href="subcategory.php">Plasztika</a></li>
                                        <li><a href="subcategory.php">Pedikűr, manikűr</a></li>
                                        <li><a href="subcategory.php">Férfi</a></li>
                                        <li><a href="subcategory.php">Egyéb</a></li>
                                    </ul>
                                </nav>
                            </div>
                            <div>
                                <div class="offersBox">
                                    <a href="offer_inner.php" class="offerItem">
                                        <figure class="imgBox">
                                            <div class="offerStatusBox">
                                                <div class="statusBox"></div>
                                                <div class="discountBox">
                                                    <p>-57%</p>
                                                </div>
                                            </div>
                                            <div class="imgCaroBox owl-carousel">
                                                <img src="https://picsum.photos/id/162/360/220" alt="">
                                            </div>
                                        </figure>
                                        <article class="textBox">
                                            <h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
                                        </article>
                                    </a>
                                    <a href="offer_inner.php" class="offerItem">
                                        
                                        <figure class="imgBox">
                                            <div class="offerStatusBox">
                                                <div class="statusBox"></div>
                                                <div class="discountBox">
                                                    <p>-57%</p>
                                                </div>
                                            </div>
                                            <div class="imgCaroBox owl-carousel">
                                                <img src="https://picsum.photos/id/163/360/220" alt="">
                                            </div>
                                        </figure>
                                        <article class="textBox">
                                            <h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
                                        </article>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="helloNachbarn dropdownParent hoverOpen">
                        <a href="hello_nachbarn.php" class="helloNachbarnBtn dropdownOpener">Grenzregionen <i class="icon icon-arrowDown"></i></a>
                        <div class="nachbarnDropdown dropdown">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur elementum massa sed lacus porttitor semper. Vestibulum libero nibh, cursus quis dui in, rutrum viverra sapien.</p>
                            <img src="https://picsum.photos/id/50/310/200" alt="">
                            <a href="hello_nachbarn.php" class="btn greenBtn block rounded">Tovább</a>
                        </div>
                    </li>
                </ul>  
            </nav>
        </div>
        
    </div>
</header>