<footer class="pageFooter">
    <div class="container">
        <div class="footerBoxes">
            <div>
                <a href="/" class="footerLogo">
                    <img src="../assets/img/logo-footer.svg" alt="SPARRR IN UNGARN">
                </a>
                <nav class="footerNav">
                    <ul>
                        <li><a href="about_us.php">Rólunk</a></li>
                        <li><a href="contact.php">Kapcsolat</a></li>
                        <li><a href="content.php">Legyél partnerünk!</a></li>
                        <li><a href="content.php">Oldaltérkép</a></li>
                    </ul>
                </nav>
            </div>
            <div>
                <h3>További linkek</h3>
                <nav class="footerNav">
                    <ul>
                        <li><a href="how_we_works.php">Hogyan működünk</a></li>
                        <li><a href="content.php">Ügyfélszolgálat</a></li>
                        <li><a href="content.php">GYIK</a></li>
                        <li><a href="content.php">ÁSZF</a></li>
                        <li><a href="content.php">Adatkezelési szabályzat</a></li>
                    </ul>
                </nav>
            </div>
            <div>
                <h3>Lépjen velünk kapcsolatba</h3>
                <p>Kövess bennünket és értesüljön az elsők között a Sparen im Urlaub ajánlatokról!</p>
                <ul class="socialMenu">
                    <li><a><i class="icon icon-mail"></i></a></li>
                    <li><a><i class="icon icon-rss"></i></a></li>
                    <li><a><i class="icon icon-twitter"></i></a></li>
                    <li><a><i class="icon icon-facebook"></i></a></li>
                    <li><a><i class="icon icon-instagram"></i></a></li>
                </ul>
            </div>
        </div>
        <p class="copyright">© 2019 SPAR IN UNGARN</p>
    </div>
</footer>