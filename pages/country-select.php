<?php include './partials/Head.php';?>

	<?php include './partials/header.php';?>

    <div class="pageContent subPageContent">
        <div class="container">

            <div class="subPageMain center">

                <h1 class="subPageTitle"><img src="../assets/img/globe.svg"> wo dein Geld mehr wert ist</h1>

                <div class="countryBox">

                    <div class="countryItem">
						<figure class="imgBox">
							<img src="../assets/img/austria.jpg" alt="">
						</figure>
						<div class="countryBody">
							<h3>Österreich</h3>
							<a class="btn greenBtn rounded block">választás</a>
						</div>
                    </div>
                    
                    <div class="countryItem">
						<figure class="imgBox">
							<img src="../assets/img/poland.jpg" alt="">
						</figure>
						<div class="countryBody">
							<h3>Polen</h3>
							<a class="btn greenBtn rounded block">választás</a>
						</div>
                    </div>
                    
                    <div class="countryItem">
						<figure class="imgBox">
							<img src="../assets/img/romania.jpg" alt="">
						</figure>
						<div class="countryBody">
							<h3>Rumänien</h3>
							<a class="btn greenBtn rounded block">választás</a>
						</div>
					</div>

					<div class="countryItem">
						<figure class="imgBox">
							<img src="../assets/img/czech-republic.jpg" alt="">
						</figure>
						<div class="countryBody">
							<h3>Tschechische Republik</h3>
							<a class="btn greenBtn rounded block">választás</a>
						</div>
					</div>

					<div class="countryItem">
						<figure class="imgBox">
							<img src="../assets/img/hungary.jpg" alt="">
						</figure>
						<div class="countryBody">
							<h3>Ungarn</h3>
							<a class="btn greenBtn rounded block">választás</a>
						</div>
					</div>

                 </div>

            </div>

        </div>
    </div>  

    <?php include './partials/footer.php';?>

	<?php include './partials/modals.php';?>

    <script src="../assets/js/plugins/jquery/jquery-3.4.1.min.js" defer></script>
    <script src="../assets/js/plugins/jquery/jquery-migrate-1.4.1.min.js" defer></script>
    <script src="../assets/js/plugins/owl-carousel/owl.carousel.min.js" defer></script>
    <script src="../assets/js/plugins/lightslider/js/lightslider.min.js" defer></script>
	<script src="../assets/js/plugins/bootstrap-modal/bootstrap-modal-min.js" defer></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
	<script src="../assets/js/plugins/lightpicker/lightpick.js" defer></script>
    <script src="../assets/js/plugins/select2/js/select2.full.min.js" defer></script>       
 
	<script src="../assets/js/main.js" defer></script>
	<script src="../assets/js/pages/country-select.js" defer></script>

<?php include './partials/Foot.php';?>