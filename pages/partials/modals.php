<div id="userModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="userLoginModal modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="modalClose" data-dismiss="modal">
                    <i class="icon icon-close3"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="loginContent">
                    <h5 class="modal-title">Bejelentkezés</h5>
                    <div class="socialLoginBox">
                        <button type="button" class="btn whiteBtn fbBtn"> <img src="../assets/img/facebook.svg" alt="facebook login"><span>Belépés Facebookkal</span></button>
                        <button type="button" class="btn whiteBtn googleBtn"> <img src="../assets/img/google.svg" alt="google login"><span>Belépés Google fiókkal</span></button>
                    </div>
                    <div class="separatorBox">
                        <hr>
                        <span>vagy</span>
                    </div>
                    <div class="formBox">
                        <h6>Írja be az adatait</h6>
                        <form id="loginForm">
                            <fieldset>
                                <div class="inputBox">
                                    <input type="email" name="email" placeholder="Email cím">
                                </div>
                                <div class="inputBox">
                                    <i class="pwVisibilityToggler icon icon-eye"></i>
                                    <input type="password" name="password" placeholder="Jelszó">
                                </div>
                            </fieldset>     
                            <div class="forgottenPwBox">
                                <a class="forgottenPwLink changeModalLink toFPW">Elfelejtett jelszó</a>
                            </div>
                            <div class="stayLoginedBox">
                                <input type="checkbox" name="stay_logined" id="stayLoginedChb" class="chbInput">
                                <label for="stayLoginedChb" class="chbLabel"><div><i class="icon icon-check"></i></div> Maradjak bejelentkezve</label>
                            </div>
                            <button type="submit" class="btn greenBtn block rounded submitBtn">Bejelentkezés</button>
                        </form>
                    </div>
                    <a class="changeModalLink toReg">Még nem regisztrált?</a>
                </div>
            </div>
                
        </div>
    </div>
    <div class="userRegModal modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="modalClose" data-dismiss="modal">
                    <i class="icon icon-close3"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="regContent">
                    <h5 class="modal-title">Regisztráció</h5>
                    <div class="socialLoginBox">
                        <button type="button" class="btn whiteBtn fbBtn"> <img src="../assets/img/facebook.svg" alt="facebook login"><span>Belépés Facebookkal</span></button>
                        <button type="button" class="btn whiteBtn googleBtn"> <img src="../assets/img/google.svg" alt="google login"><span>Belépés Google fiókkal</span></button>
                    </div>
                    <div class="separatorBox">
                        <hr>
                        <span>vagy</span>
                    </div>
                    <div class="formBox">
                        <h6>Írja be az adatait</h6>
                        <form id="regForm">
                            <fieldset>
                                <div class="inputRow">
                                    <div class="inputBox">
                                        <input type="text" name="lastname" placeholder="Vezetéknév">
                                    </div>
                                    <div class="inputBox">
                                        <input type="text" name="firstname" placeholder="Keresztnév">
                                    </div>
                                </div>
                                <div class="inputBox">
                                    <input type="email" name="email" placeholder="Email cím">
                                </div>
                                <div class="inputBox">
                                    <i class="pwVisibilityToggler icon icon-eye"></i>
                                    <input type="password" name="password" placeholder="Jelszó">
                                </div>
                                <div class="inputBox">
                                    <i class="pwVisibilityToggler icon icon-eye"></i>
                                    <input type="password" name="password_re" placeholder="Jelszó mégegyszer">
                                </div>
                                <div class="inputBox locBox">
                                    <i class="icon icon-pin"></i>
                                    <select>
                                        <option></option>
                                        <option value="Budapest">Budapest</option>
                                        <option value="Pécs">Pécs</option>
                                        <option value="Szeged">Szeged</option>
                                    </select>
                                    <i class="icon icon-cancel"></i>
                                </div>
                            </fieldset>
                            <div class="genderRadioBox">
                                <span>Nemed:</span>
                                <div>
                                    <input type="radio" name="gender" id="femaleRadio" class="radioInput">
                                    <label for="femaleRadio" class="radioLabel"><div><span></span></div> Nő</label>
                                </div>
                                <div>
                                    <input type="radio" name="gender" id="maleRadio" class="radioInput">
                                    <label for="maleRadio" class="radioLabel"><div><span></span></div> Férfi</label>
                                </div>
                            </div>
                            <div>
                                <input type="checkbox" name="newsletter" id="newsletterChb" class="chbInput">
                                <label for="newsletterChb" class="chbLabel"><div><i class="icon icon-check"></i></div> Feliratkozom a hírlevélre, hogy elsőként értesüljek az újdonságokról.</label>
                            </div>
                            <button type="submit" class="btn greenBtn block rounded submitBtn">Fiók létrehozása</button>
                        </form>
                    </div>
                    <a class="changeModalLink toLogin">Van már fiókod?</a>
                </div>
            </div>
                
        </div>
    </div>
    <div class="forgottenPwModal modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="modalClose" data-dismiss="modal">
                    <i class="icon icon-close3"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="forgottenPwContent">
                    <h5 class="modal-title">Elfelejtett jelszó</h5>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    <div class="formBox">
                        <form id="forgottenPwForm">
                            <fieldset>
                                <div class="inputBox">
                                    <input type="email" name="email" placeholder="Email cím">
                                </div>
                            </fieldset>
                            <button type="submit" class="btn greenBtn block rounded submitBtn">Küldés</button>
                        </form>
                    </div>
                </div>
            </div>
                
        </div>
    </div>
</div>

<div id="sendInEmailModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="modalClose" data-dismiss="modal">
                    <i class="icon icon-close3"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="sendInEmailContent">
                    <h5 class="modal-title">Küldés emailben</h5>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    <div class="formBox">
                        <form id="sendInEmailForm">
                            <fieldset>
                                <div class="inputBox">
                                    <input type="email" name="email" placeholder="Email cím">
                                </div>
                            </fieldset>
                            <button type="submit" class="btn greenBtn block rounded submitBtn">Küldés</button>
                        </form>
                    </div>
                </div>
            </div>
                
        </div>
    </div>
</div>