<?php include './partials/Head.php';?>

	<?php include './partials/header.php';?>

    <div class="pageContent subPageContent">
        <div class="container">

            <h1 class="subPageTitle">Tartalmi</h1>

            <div class="subPageMain center">

                <article class="mainTextBox">
                    <h1>Header 1</h1>
                    <h2>Header 2</h2>
                    <h3>Header 3</h3>
                    <h4>Header 4</h4>
                    <h5>Header 5</h5>
                    <h6>Header 6</h6>

                    <br><br>

                    <hr>

                    <br><br>

                    <p>Ut tempus dolor in mollis egestas. Curabitur <strong>rhoncus fermentum</strong> nisi, a ultricies nulla dapibus eget. Mauris diam enim, vehicula at ultricies ac, fringilla quis ex. Suspendisse <em>facilisis</em> consectetur magna et ultricies. Vivamus <span>sapien</span> dolor, fringilla <sup>vitae</sup> facilisis eu, <sub>placerat</sub> nec turpis.</p>

                        <br><br>

                        <blockquote>"In porttitor, lorem quis tincidunt bibendum, nulla arcu molestie risus..."</blockquote>

                    <br><br>

                    <img src="https://picsum.photos/id/155/840/440" alt="gallery">

                    <br><br>

                    <ul>
                        <li>Lista 1</li>
                        <li>Lista 2</li>
                        <li>Lista 3</li>
                        <li>Lista 4</li>
                        <li>Lista 5</li>
                    </ul>

                    <br><br>

                    <ol>
                        <li>Lista 1</li>
                        <li>Lista 2</li>
                        <li>Lista 3</li>
                        <li>Lista 4</li>
                        <li>Lista 5</li>
                    </ol>

                    <br><br>

                    <a href="/">https://www.sparrrungarn.com</a>

                    <br><br>

                    <ul class="contentGalleryList">
                        <li data-src="https://picsum.photos/id/121/1920/900" data-thumb="https://picsum.photos/id/121/168/96">
                            <img src="https://picsum.photos/id/121/840/480" alt="gallery img01">
                        </li>
                        <li data-src="https://picsum.photos/id/131/1920/900" data-thumb="https://picsum.photos/id/131/168/96">
                            <img src="https://picsum.photos/id/131/840/480" alt="gallery img01">
                        </li>
                        <li data-src="https://picsum.photos/id/141/1920/900" data-thumb="https://picsum.photos/id/141/168/96">
                            <img src="https://picsum.photos/id/141/840/480" alt="gallery img01">
                        </li>
                        <li data-src="https://picsum.photos/id/151/1920/900" data-thumb="https://picsum.photos/id/151/168/96">
                            <img src="https://picsum.photos/id/151/840/480" alt="gallery img01">
                        </li>
                        <li data-src="https://picsum.photos/id/161/1920/900" data-thumb="https://picsum.photos/id/161/168/96">
                            <img src="https://picsum.photos/id/161/840/480" alt="gallery img01">
                        </li>
                        <li data-src="https://picsum.photos/id/171/1920/900" data-thumb="https://picsum.photos/id/171/168/96">
                            <img src="https://picsum.photos/id/171/840/480" alt="gallery img01">
                        </li>
                        <li data-src="https://picsum.photos/id/181/1920/900" data-thumb="https://picsum.photos/id/181/168/96">
                            <img src="https://picsum.photos/id/181/840/480" alt="gallery img01">
                        </li>
                    </ul>

                </article>
            </div>

        </div>
    </div>

    <?php include './partials/footer.php';?>

	<?php include './partials/modals.php';?>

    <script src="../assets/js/plugins/jquery/jquery-3.4.1.min.js" defer></script>
    <script src="../assets/js/plugins/jquery/jquery-migrate-1.4.1.min.js" defer></script>
    <script src="../assets/js/plugins/owl-carousel/owl.carousel.min.js" defer></script>
    <script src="../assets/js/plugins/lightslider/js/lightslider.min.js" defer></script>
	<script src="../assets/js/plugins/bootstrap-modal/bootstrap-modal-min.js" defer></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
	<script src="../assets/js/plugins/lightpicker/lightpick.js" defer></script>
    <script src="../assets/js/plugins/select2/js/select2.full.min.js" defer></script>       
    <script src="../assets/js/plugins/lightgallery/js/lightgallery.min.js" defer></script>                

	<script src="../assets/js/main.js" defer></script>
	<script src="../assets/js/pages/content.js" defer></script>

<?php include './partials/Foot.php';?>