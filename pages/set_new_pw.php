<?php include './partials/Head.php';?>

    <div class="loginAndRegPageContent">
        <figure class="titleBox">
            <a href="/"><img src="../assets/img/logo-white.svg" alt="SPARRR logo"></a>
            <img src="https://picsum.photos/id/155/600/1080" alt="">
            <h2>Suspendisse quisnisl cursus bibendum</h2>
        </figure>
        <div class="mainContent">
            <a class="tabletLogo"><img src="../assets/img/logo-color.svg" alt="SPARRR logo"></a>
            <div class="loginContent">
                <h1>Új jelszó beállítása</h1>
                <div class="formBox setNewPwBox">
                    <form id="setNewPwForm">
                        <fieldset>
                            <div class="inputBox">
                                <i class="pwVisibilityToggler icon icon-eye"></i>
                                <input type="password" name="password" placeholder="Jelszó">
                            </div>
                            <div class="inputBox">
                                <i class="pwVisibilityToggler icon icon-eye"></i>
                                <input type="password" name="password_re" placeholder="Jelszó mégegyszer">
                            </div>
                        </fieldset>     
                        <button type="submit" class="btn greenBtn rounded submitBtn">Jelszó beállítása</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <?php include './partials/modals.php';?>

    <script src="../assets/js/plugins/jquery/jquery-3.4.1.min.js" defer></script>
    <script src="../assets/js/plugins/jquery/jquery-migrate-1.4.1.min.js" defer></script>
	<script src="../assets/js/plugins/bootstrap-modal/bootstrap-modal-min.js" defer></script>

	<script src="../assets/js/plugins/select2/js/select2.full.min.js" defer></script>    

	<script src="../assets/js/main.js" defer></script>

<?php include './partials/Foot.php';?>