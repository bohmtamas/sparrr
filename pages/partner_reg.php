<?php include './partials/Head.php';?>

    <div class="loginAndRegPageContent partner">
        <figure class="titleBox">
            <a href="/"><img src="../assets/img/logo-white.svg" alt="SPARRR logo"></a>
            <img src="https://picsum.photos/id/155/600/1080" alt="">
            <h2>Suspendisse quisnisl cursus bibendum</h2>
        </figure>
        <div class="mainContent partnerReg">
            <a class="tabletLogo"><img src="../assets/img/logo-color.svg" alt="SPARRR logo"></a>
            <div class="regContent">
                <h1>Partner regisztráció</h1>
                <div class="formBox">
                    <form id="partnerRegForm">
                        <fieldset>
                            <legend>Cég adatok</legend>
                            <div class="inputRow">
                                <div class="inputBox">
                                    <input type="text" name="shop_name" placeholder="Üzletnév">
                                </div>
                                <div class="inputBox">
                                    <input type="text" name="company_name" placeholder="Cégnév">
                                </div>
                            </div>
                            <div class="inputRow">
                                <div class="inputBox">
                                    <input type="text" name="shop_address" placeholder="Üzlet címe">
                                </div>
                            </div>
                            <div class="inputRow">
                                <div class="inputBox">
                                    <input type="text" name="phone" placeholder="Telefon">
                                </div>
                                <div class="inputBox">
                                    <input type="email" name="email" placeholder="Email cím">
                                </div>
                                <div class="inputBox">
                                    <input type="text" name="web" placeholder="Webcím">
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <legend>Kapcsolattartó</legend>
                            <div class="inputRow">
                                <div class="inputBox">
                                    <input type="text" name="contact_lastname" placeholder="Vezetéknév">
                                </div>
                                <div class="inputBox">
                                    <input type="text" name="contact_firstname" placeholder="Keresztnév">
                                </div>
                            </div>
                            <div class="inputRow">
                                <div class="inputBox">
                                    <input type="text" name="contact_phone" placeholder="Telefon">
                                </div>
                                <div class="inputBox">
                                    <input type="email" name="contact_email" placeholder="Email cím">
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <legend>Számlázási adatok</legend>
                            <div class="inputRow">
                                <input type="checkbox" name="address_diff" id="addressDiffChb" class="chbInput">
                                <label for="addressDiffChb" class="chbLabel"><div><i class="icon icon-check"></i></div> Számlázási cím eltér?</label>
                            </div>
                            <div class="inputRow billingAddress">
                                <div class="inputBox">
                                    <input type="text" name="billing_address" placeholder="Számlázási cím">
                                </div>
                            </div>
                            <div class="inputRow">
                                <div class="inputBox">
                                    <input type="text" name="company_reg_number" placeholder="Cégjegyzékszám">
                                </div>
                                <div class="inputBox">
                                    <input type="text" name="tax_number" placeholder="Adószám">
                                </div>
                            </div>
                            <div class="inputRow">
                                <div class="inputBox">
                                    <input type="text" name="bank_name" placeholder="Számlavezető bank">
                                </div>
                                <div class="inputBox">
                                    <input type="text" name="bank_number" placeholder="Számlaszám">
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <div class="inputBox">
                                <input type="text" name="username" placeholder="Felhasználónév">
                            </div>
                            <div class="inputBox">
                                <i class="pwVisibilityToggler icon icon-eye"></i>
                                <input type="password" name="password" placeholder="Jelszó">
                            </div>
                            <div class="inputBox">
                                <i class="pwVisibilityToggler icon icon-eye"></i>
                                <input type="password" name="password_again" placeholder="Jelszó mégegyszer">
                            </div>
                            <div class="inputBox">
                                <input type="checkbox" name="aszf" id="aszfChb" class="chbInput">
                                <label for="aszfChb" class="chbLabel"><div><i class="icon icon-check"></i></div> ÁSZF lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse nisl dui, bibendum at turpis vel, sagittis tristique nisl. Nullam tempus orci quis quam porttitor tristique.</label>
                            </div>
                            <div class="inputBox">
                                <input type="checkbox" name="newsletter" id="newsletterChb" class="chbInput">
                                <label for="newsletterChb" class="chbLabel"><div><i class="icon icon-check"></i></div> Feliratkozom a hírlevélre, hogy elsőként értesüljek az újdonságokról.</label>
                            </div>
                            <div class="inputBox">
                                <input type="checkbox" name="terms_conditions" id="termsChb" class="chbInput">
                                <label for="termsChb" class="chbLabel"><div><i class="icon icon-check"></i></div> Adathasználati feltételek elfogadása</label>
                            </div>
                        </fieldset>
                        <button type="submit" class="btn greenBtn rounded submitBtn">Fiók létrehozása</button>
                    </form>
                </div>
                <a href="partner_login.php" class="changePageLink">Van már partner fiókod?</a>
			</div>
			<div class="thanksBox">
				<h3>Köszönjük, hogy partnerünk lett!</h3>
				<p>Emailben elküldtük a visszaigazolást…Proin eleifend, libero at hendrerit convallis, leo purus auctor risus, vel sodales enim massa ac risus. Curabitur porta justo ut odio rhoncus luctus. Quisque bibendum fringilla luctus.</p>
				<a class="btn greenBtn rounded">Rendben</a>
            </div>
            <div class="toLoginPageBox">
                <span>Van már partner fiókod?</span>
                <a href="partner_login.php" class="btn greyBtn rounded">Bejelentkezés</a>
            </div>
        </div>
        
    </div>

    <?php include './partials/modals.php';?>  

    <script src="../assets/js/plugins/jquery/jquery-3.4.1.min.js" defer></script>
    <script src="../assets/js/plugins/jquery/jquery-migrate-1.4.1.min.js" defer></script>

	<script src="../assets/js/plugins/select2/js/select2.full.min.js" defer></script>    

	<script src="../assets/js/main.js" defer></script>
	<script src="../assets/js/pages/partner_registration.js" defer></script>

<?php include './partials/Foot.php';?>