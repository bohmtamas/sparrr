<?php include './partials/Head.php';?>

	<?php include './partials/header.php';?>

    <div class="pageContent subPageContent offerDetailsContent">
        <div class="container">

            <h1 class="subPageTitle">Global Travel And Vacations Luxury Travel On A Tight Budget</h1>

            <div class="splitedWrapper offerTopBox">

                <div class="subPageMain">

                    <div class="offerStatusBox">
                        <div class="statusBox"></div>
                        <div class="discountBox">
                            <p>-57%</p>
                        </div>
                    </div>

                    <ul class="contentGalleryList">
                        <li data-src="https://picsum.photos/id/121/1920/900" data-thumb="https://picsum.photos/id/121/168/96">
                            <img src="https://picsum.photos/id/121/840/480" alt="gallery img01">
                        </li>
                        <li data-src="https://picsum.photos/id/131/1920/900" data-thumb="https://picsum.photos/id/131/168/96">
                            <img src="https://picsum.photos/id/131/840/480" alt="gallery img01">
                        </li>
                        <li data-src="https://picsum.photos/id/141/1920/900" data-thumb="https://picsum.photos/id/141/168/96">
                            <img src="https://picsum.photos/id/141/840/480" alt="gallery img01">
                        </li>
                        <li data-src="https://picsum.photos/id/151/1920/900" data-thumb="https://picsum.photos/id/151/168/96">
                            <img src="https://picsum.photos/id/151/840/480" alt="gallery img01">
                        </li>
                        <li data-src="https://picsum.photos/id/161/1920/900" data-thumb="https://picsum.photos/id/161/168/96">
                            <img src="https://picsum.photos/id/161/840/480" alt="gallery img01">
                        </li>
                        <li data-src="https://picsum.photos/id/171/1920/900" data-thumb="https://picsum.photos/id/171/168/96">
                            <img src="https://picsum.photos/id/171/840/480" alt="gallery img01">
                        </li>
                        <li data-src="https://picsum.photos/id/181/1920/900" data-thumb="https://picsum.photos/id/181/168/96">
                            <img src="https://picsum.photos/id/181/840/480" alt="gallery img01">
                        </li>
                    </ul>

                </div>

                <aside class="subPageAside right">
    
                    <div class="offerMainInfos">
                        <p>Suspendisse in eros nec ante bibendum tempor in feugiat neque. Donec vel molestie ligula. Maecenas aliquet egestas sodales. Etiam enim felis, tristique at maximus id, porta at magna. Nunc leo tortor, placerat quis lectus efficitur, ornare congue erat. Aliquam erat volutpat. Nam vel nulla non ligula congue rhoncus. Fusce semper luctus imperdiet.</p>
                        <a class="btn greenBtn rounded block printItBtn">Nyomtatás</a>
                        <!-- HA BE VAN JELENTKEZVE ELEJE -->
                        <a class="btn greyBtn rounded block" href="#sendInEmailModal" data-toggle="modal">Küldés emailben</a>
                        <!-- HA BE VAN JELENTKEZVE VÉGE -->
                        <!-- HA NINCS BEJELENTKEZVE ELEJE
                        <a data-toggle="modal" href="#userModal" class="btn greyBtn rounded block saveOfferBtn openLogin"> <i class="icon icon-banner"></i> Ajánlat mentése</a>
                             HA NINCS BEJELENTKEZVE VÉGE -->
                        <h5><i class="icon icon-profile"></i>98 vásárló</h5>
                        <!--
                        <div class="shareBox">
                            <ul>
                                <li><a class="st-custom-button" data-network="facebook"><i class="icon icon-facebook-share"></i>Megosztás</a></li>
                                <li><a class="st-custom-button" data-network="twitter"><i class="icon icon-twitter"></i>Megosztás</a></li>
                            </ul>
                        </div>
                        -->
                    </div>

                </aside>

            </div>

            <div class="subPageMain">

                <div class="offerDetailsBox">
                    <div class="mainTextBox">
                        <h3>Jó, ha tudod</h3>
                        <br>
                        <p>Phasellus tempor lectus a tortor lobortis varius. Praesent magna tellus, mollis at iaculis ut, euismod id justo. Nulla imperdiet mollis commodo. Sed tincidunt, ante id rhoncus aliquet, nisi lectus interdum urna, sit amet suscipit felis sapien sit amet ante. Donec eu nibh a leo venenatis fermentum. Fusce bibendum metus sit amet ipsum auctor accumsan. Donec ut risus vel purus mollis malesuada ac eget enim. Suspendisse potenti. Nunc sit amet eros vitae lectus placerat ultrices eu sit amet ipsum. Sed gravida libero ac ex malesuada elementum. Nulla placerat vulputate mi, in egestas sapien consectetur et.</p>
                    </div>
                </div>

                <div class="offerDetailsBox">
                    <div class="mainTextBox">
                        <h3>Az ajánlat tartalma:</h3>
                        <br>
                        <p>Phasellus tempor lectus a tortor lobortis varius. Praesent magna tellus, mollis at iaculis ut, euismod id justo. Nulla imperdiet mollis commodo. Sed tincidunt, ante id rhoncus aliquet, nisi lectus interdum urna, sit amet suscipit felis sapien sit amet ante. Donec eu nibh a leo venenatis fermentum. Fusce bibendum metus sit amet ipsum auctor accumsan. Donec ut risus vel purus mollis malesuada ac eget enim. Suspendisse potenti. Nunc sit amet eros vitae lectus placerat ultrices eu sit amet ipsum. Sed gravida libero ac ex malesuada elementum. Nulla placerat vulputate mi, in egestas sapien consectetur et.</p>
                    </div>
                </div>

                <div class="offerDetailsBox">
                    <div class="mainTextBox">
                        <h3>Fizetési lehetőségek</h3>
                        <br>
                        <p>Phasellus tempor lectus a tortor lobortis varius. Praesent magna tellus, mollis at iaculis ut, euismod id justo. Nulla imperdiet mollis commodo.</p>
                    </div>
                </div>

                <div class="offerDetailsBox">
                    <div class="mainTextBox">
                        <h3>Érvényesség</h3>
                        <br>
                        <p>Az utalvány felhasználható 2019. április 30-ig, korlátozott számban hétköznapokon és hétvégén is. Kiemelt (03.15.-17.) és ünnepi időszakban (04.18.-22.) felár ellenében a szabad helyek függvényében, a szállodával előre egyeztetett időpontban, írásos visszaigazolás alapján. A húsvéti időszakban minimum 3 éjszakára foglalható.</p>
                    </div>
                </div>

                <div class="offerDetailsBox">
                    <div class="mainTextBox">
                        <h3>Helyszín</h3>
                        <br>
                        <p>Phasellus tempor lectus a tortor lobortis varius. Praesent magna tellus, mollis at iaculis ut, euismod id justo.</p>
                    </div>
                    <div id="mapBox" data-map-coor-x="0" data-map-coor-y="0" data-map-zoom-value="14"></div>
                </div>

                <div class="offerDetailsBox">
                    <div class="mainTextBox">
                        <h3>Nyitvatartás</h3>
                        <br>
                        <p>Phasellus tempor lectus a tortor lobortis varius. Praesent magna tellus, mollis at iaculis ut, euismod id justo. Nulla imperdiet mollis commodo.</p>
                    </div>
                </div>

            </div>

            <div class="subOffers">
                <h5>Ez is érdekelheti</h5>
                <div class="offersBox owl-carousel">
                    <a class="offerItem">
                        <figure class="imgBox">
                            <div class="offerStatusBox">
                                <div class="statusBox"></div>
                                <div class="discountBox">
                                    <p>-57%</p>
                                </div>
                            </div>
                            <div class="imgCaroBox">
                                <img src="https://picsum.photos/id/160/360/220" alt="">
                            </div>
                        </figure>
                        <article class="textBox">
                            <h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
                            <div class="offerPriceBox">
                                <strong class="newPrice">45.000 Ft</strong>
                                <p class="oldPrice">79.000 Ft</p>
                            </div>
                        </article>
                        <div class="distanceBox">
                            <span>2.8 km</span>
                            <span class="saveOffer"><i class="icon icon-banner"></i></span>
                        </div>
                    </a>
                    <a class="offerItem">
                        <figure class="imgBox">
                            <div class="offerStatusBox">
                                <div class="statusBox"></div>
                                <div class="discountBox">
                                    <p>-57%</p>
                                </div>
                            </div>
                            <div class="imgCaroBox">
                                <img src="https://picsum.photos/id/161/360/220" alt="">
                            </div>
                        </figure>
                        <article class="textBox">
                            <h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
                            <div class="offerPriceBox">
                                <strong class="newPrice">45.000 Ft</strong>
                                <p class="oldPrice">79.000 Ft</p>
                            </div>
                        </article>
                        <div class="distanceBox">
                            <span>2.8 km</span>
                            <span class="saveOffer"><i class="icon icon-banner"></i></span>
                        </div>
                    </a>
                    <a class="offerItem">
                        <figure class="imgBox">
                            <div class="offerStatusBox">
                                <div class="statusBox"></div>
                                <div class="discountBox">
                                    <p>-57%</p>
                                </div>
                            </div>
                            <div class="imgCaroBox">
                                <img src="https://picsum.photos/id/162/360/220" alt="">
                            </div>
                        </figure>
                        <article class="textBox">
                            <h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
                            <div class="offerPriceBox">
                                <strong class="newPrice">45.000 Ft</strong>
                                <p class="oldPrice">79.000 Ft</p>
                            </div>
                        </article>
                        <div class="distanceBox">
                            <span>2.8 km</span>
                            <span class="saveOffer"><i class="icon icon-banner"></i></span>
                        </div>
                    </a>
                    <a class="offerItem">
                        
                        <figure class="imgBox">
                            <div class="offerStatusBox">
                                <div class="statusBox"></div>
                                <div class="discountBox">
                                    <p>-57%</p>
                                </div>
                            </div>
                            <div class="imgCaroBox">
                                <img src="https://picsum.photos/id/163/360/220" alt="">
                            </div>
                        </figure>
                        <article class="textBox">
                            <h4>Global Travel And Vacations Luxury Travel On A Tight Budget</h4>
                            <div class="offerPriceBox">
                                <strong class="newPrice">45.000 Ft</strong>
                                <p class="oldPrice">79.000 Ft</p>
                            </div>
                        </article>
                        <div class="distanceBox">
                            <span>2.8 km</span>
                            <span class="saveOffer"><i class="icon icon-banner"></i></span>
                        </div>
                    </a>
                    
                </div>
            </div>

        </div>
    </div>

    <?php include './partials/footer.php';?>

    <?php include './partials/modals.php';?>

    <script src="https://maps.googleapis.com/maps/api/js?key=" defer></script>
    
    <script src="../assets/js/plugins/jquery/jquery-3.4.1.min.js" defer></script>
    <script src="../assets/js/plugins/jquery/jquery-migrate-1.4.1.min.js" defer></script>
    <script src="../assets/js/plugins/owl-carousel/owl.carousel.min.js" defer></script>
    <script src="../assets/js/plugins/lightslider/js/lightslider.min.js" defer></script>
	<script src="../assets/js/plugins/bootstrap-modal/bootstrap-modal-min.js" defer></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
    <script src="../assets/js/plugins/lightpicker/lightpick.js" defer></script>
	<script src="../assets/js/plugins/select2/js/select2.full.min.js" defer></script>            
    <script src="../assets/js/plugins/lightgallery/js/lightgallery.min.js" defer></script>            

	<script src="../assets/js/main.js" defer></script>
	<script src="../assets/js/pages/adpage.js" defer></script>

<?php include './partials/Foot.php';?>