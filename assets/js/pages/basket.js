var Basket = {}
 
Basket.init = function(){

    this.toggleGiftBoxVisibility();
}

Basket.toggleGiftBoxVisibility = function(){
    $("body").on("change", ".giftWrapper .chbInput", function(){
        if($(this).is(":checked")){
            $(this).parents(".giftWrapper").find(".giftedDataBox").css("display","block");
        }
        else {
            $(this).parents(".giftWrapper").find(".giftedDataBox").css("display","none");
        }
    });
}

$(document).ready(function(){
    Basket.init();
});