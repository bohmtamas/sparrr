<?php include './partials/Head.php';?>

	<?php include './partials/header.php';?>

    <div class="pageContent subPageContent">
        <div class="container">

            <h1 class="subPageTitle">404 Hibaoldal</h1>

            <div class="subPageMain center">

                <article class="mainTextBox">

                    <p>Ut tempus dolor in mollis egestas. Curabitur <strong>rhoncus fermentum</strong> nisi, a ultricies nulla dapibus eget. Mauris diam enim, vehicula at ultricies ac, fringilla quis ex. Suspendisse <em>facilisis</em> consectetur magna et ultricies. Vivamus <span>sapien</span> dolor, fringilla <sup>vitae</sup> facilisis eu, <sub>placerat</sub> nec turpis.</p>

                    <br><br>

                    <p>Ut tempus dolor in mollis egestas. Curabitur <strong>rhoncus fermentum</strong> nisi, a ultricies nulla dapibus eget. Mauris diam enim, vehicula at ultricies ac, fringilla quis ex. Suspendisse <em>facilisis</em> consectetur magna et ultricies. Vivamus <span>sapien</span> dolor, fringilla <sup>vitae</sup> facilisis eu, <sub>placerat</sub> nec turpis.</p>

                    <br><br>

                </article>
            </div>

        </div>
    </div>

    <?php include './partials/footer.php';?>

	<?php include './partials/modals.php';?>

    <script src="../assets/js/plugins/jquery/jquery-3.4.1.min.js" defer></script>
    <script src="../assets/js/plugins/jquery/jquery-migrate-1.4.1.min.js" defer></script>
    <script src="../assets/js/plugins/owl-carousel/owl.carousel.min.js" defer></script>
    <script src="../assets/js/plugins/lightslider/js/lightslider.min.js" defer></script>
	<script src="../assets/js/plugins/bootstrap-modal/bootstrap-modal-min.js" defer></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
	<script src="../assets/js/plugins/lightpicker/lightpick.js" defer></script>
    <script src="../assets/js/plugins/select2/js/select2.full.min.js" defer></script>       
    <script src="../assets/js/plugins/lightgallery/js/lightgallery.min.js" defer></script>                

	<script src="../assets/js/main.js" defer></script>
	<script src="../assets/js/pages/content.js" defer></script>

<?php include './partials/Foot.php';?>