<?php include './partials/Head.php';?>

    <div class="loginAndRegPageContent">
        <figure class="titleBox">
            <a href="/"><img src="../assets/img/logo-white.svg" alt="SPARRR logo"></a>
            <img src="https://picsum.photos/id/155/600/1080" alt="">
            <h2>Sparen über all!</h2>
        </figure>
        <div class="mainContent">
            <a class="tabletLogo"><img src="../assets/img/logo-color.svg" alt="SPARRR logo"></a>
            <div class="regContent">
                <h1>Regisztráció</h1>
                <div class="socialLoginBox">
                    <button type="button" class="btn whiteBtn fbBtn"> <img src="../assets/img/facebook.svg" alt="facebook login"><span>Belépés Facebookkal</span></button>
                    <button type="button" class="btn whiteBtn googleBtn"> <img src="../assets/img/google.svg" alt="google login"><span>Belépés Google fiókkal</span></button>
                </div>
                <div class="separatorBox">
                    <hr>
                    <span>vagy</span>
                </div>
                <div class="formBox">
                    <h6>Kérem írja be adatait</h6>
                    <form id="regForm">
                        <fieldset>
                            <div class="inputRow">
                                <div class="inputBox">
                                    <input type="text" name="firstname" placeholder="Keresztnév">
                                </div>
                                <div class="inputBox">
                                    <input type="text" name="lastname" placeholder="Vezetéknév">
                                </div>
                            </div>
                            <div class="inputBox">
                                <input type="email" name="email" placeholder="Email">
                            </div>
                            <div class="inputBox">
                                <i class="pwVisibilityToggler icon icon-eye"></i>
                                <input type="password" name="password" placeholder="Jelszó">
                            </div>
                            <div class="inputBox">
                                <i class="pwVisibilityToggler icon icon-eye"></i>
                                <input type="password" name="password_re" placeholder="Jelszó mégegyszer">
                            </div>
                            <div class="inputBox locBox">
                                <i class="icon icon-pin"></i>
                                <select>
                                    <option></option>
                                    <option value="Budapest">Budapest</option>
                                    <option value="Pécs">Pécs</option>
                                    <option value="Szeged">Szeged</option>
                                </select>
                                <i class="icon icon-cancel"></i>

                            </div>
                        </fieldset>
                        <div class="genderRadioBox">
                            <span>Nemed:</span>
                            <div>
                                <input type="radio" name="gender" id="femaleRadio" class="radioInput">
                                <label for="femaleRadio" class="radioLabel"><div><span></span></div> Nő</label>
                            </div>
                            <div>
                                <input type="radio" name="gender" id="maleRadio" class="radioInput">
                                <label for="maleRadio" class="radioLabel"><div><span></span></div> Férfi</label>
                            </div>
                        </div>
                        <div class="inputBox">
                            <input type="checkbox" name="newsletter" id="newsletterChb" class="chbInput">
                            <label for="newsletterChb" class="chbLabel"><div><i class="icon icon-check"></i></div> Feliratkozom a hírlevélre, hogy elsőként értesüljek az újdonságokról.</label>
                        </div>
                        <div>
                            <input type="checkbox" name="aszf" id="acceptAszfChb" class="chbInput">
                            <label for="acceptAszfChb" class="chbLabel"><div><i class="icon icon-check"></i></div> Regisztrációmmal elfogadom az <a>ÁSZF</a>-et és az <a>Adatvédelmi nyilatkozat</a>-ot.</label>
                        </div>
                        <button type="submit" class="btn greenBtn rounded submitBtn">Fiók létrehozása</button>
                    </form>
                </div>
                <a href="partner_reg.php" class="changePageLink">Partnerként regisztrálnál?</a>
            </div>
            <div class="toLoginPageBox">
                <span>Van már fiókod?</span>
                <a href="login.php" class="btn greyBtn rounded">Bejelentkezés</a>
            </div>
        </div>  
    </div>

    <?php include './partials/modals.php';?>  

    <script src="https://www.google.com/recaptcha/api.js?render=6LeNcqsUAAAAAJb7itmiDAaI5REARMahmif-Uzie"></script>

    <script src="../assets/js/plugins/jquery/jquery-3.4.1.min.js" defer></script>
    <script src="../assets/js/plugins/jquery/jquery-migrate-1.4.1.min.js" defer></script>

	<script src="../assets/js/plugins/select2/js/select2.full.min.js" defer></script>    

	<script src="../assets/js/main.js" defer></script>
    <script src="../assets/js/pages/registration.js" defer></script>
    
    <script>
	grecaptcha.ready(function() {
		grecaptcha.execute('6LeNcqsUAAAAAJb7itmiDAaI5REARMahmif-Uzie', {action: 'regForm'}).then(function(token) {
			
		});
	});
	</script>

<?php include './partials/Foot.php';?>