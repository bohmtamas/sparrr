<?php include './partials/Head.php';?>

	<?php include './partials/header.php';?>

    <div class="pageContent subPageContent">
        <div class="container">

            <h1 class="subPageTitle">Ügyfélszolgálat / Kapcsolat</h1>

            <div class="subPageMain full flex mobileReverse">

                <div class="contactFormBox">
					<form id="contactForm">
						<div class="inputBox">
							<label>Név</label>
							<input type="text" name="name">
						</div>
						<div class="inputBox">
							<label>Email cím</label>
							<input type="email" name="email">
						</div>
						<div class="inputBox">
							<label>Tárgy</label>
							<input type="text" name="subject">
						</div>
						<div class="inputBox">
							<label>Üzenet</label>
							<textarea></textarea>
						</div>
						<div class="inputBox">
							<input type="checkbox" name="contact_newsletter" id="contact_newsletterChb" class="chbInput">
							<label for="contact_newsletterChb" class="chbLabel"><div><i class="icon icon-check"></i></div> Feliratkozom a hírlevélre, hogy elsőként értesüljek az újdonságokról.</label>
						</div>
						<div class="inputBox">
                            <input type="checkbox" name="aszf" id="acceptAszfChb" class="chbInput">
                            <label for="acceptAszfChb" class="chbLabel"><div><i class="icon icon-check"></i></div> Elfogadom az <a>ÁSZF</a>-et és az <a>Adatvédelmi nyilatkozat</a>-ot.</label>
                        </div>
						<div class="inputBox">
							<button type="submit" class="btn greenBtn rounded submitBtn">Küldés</button>
						</div>
					</form>
				</div>
				
				<div class="contactInfoBox">
					<h6>Sparen im Urlaub</h6>
					<hr>
					<p>H-1037 Budapest, Montevideo utca 3/b.</p>
					<a href="mailto:info@sparenimurlaub.de">info@sparenimurlaub.de</a>
				</div>

            </div>

        </div>
    </div>

	<?php include './partials/footer.php';?>

	<?php include './partials/modals.php';?>

	<script src="https://www.google.com/recaptcha/api.js?render=6LeNcqsUAAAAAJb7itmiDAaI5REARMahmif-Uzie"></script>

    <script src="../assets/js/plugins/jquery/jquery-3.4.1.min.js" defer></script>
    <script src="../assets/js/plugins/jquery/jquery-migrate-1.4.1.min.js" defer></script>
    <script src="../assets/js/plugins/owl-carousel/owl.carousel.min.js" defer></script>
	<script src="../assets/js/plugins/bootstrap-modal/bootstrap-modal-min.js" defer></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
	<script src="../assets/js/plugins/lightpicker/lightpick.js" defer></script>
	<script src="../assets/js/plugins/select2/js/select2.full.min.js" defer></script>        

	<script src="../assets/js/main.js" defer></script>

	<script>
	grecaptcha.ready(function() {
		grecaptcha.execute('6LeNcqsUAAAAAJb7itmiDAaI5REARMahmif-Uzie', {action: 'contactForm'}).then(function(token) {
			
		});
	});
	</script>

<?php include './partials/Foot.php';?>